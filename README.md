# RCbenchmark.com configuration utility

**Crossplatform control software for the RCbenchmark.com motor test tool**

This GUI is needed to interface with the RCbenchmark.com USB test tool device. With it you can:

-See real-time sensor plots<br />
-Record data to CSV files<br />
-Control your ESC and servo motors<br />
-Upgrade board firmware

[![available in the Chrome web store](GUI.png)](https://chrome.google.com/webstore/detail/rcbenchmarkcom-gui/loaadjknlfcpljcickkiogkbmollildg)

## Authors

Developped and maintained by Tyto Robotics Inc. This software is written to support the [RCbenchmark.com](http://www.RCbenchmark.com) USB test tool. This test tool was originally a [fork](#credits) of the open source project Cleanflight (see credits below).

## Installation

### Via chrome webstore

1. Visit [Chrome web store](https://chrome.google.com/webstore/detail/rcbenchmarkcom-gui/loaadjknlfcpljcickkiogkbmollildg)
2. Click **+ Free**

As long as you remain online, the application will automatically update itself when new versions are released.

## Credits

Dominic Clifton/hydra - maintainer of the Cleanflight firmware and configurator, from which this project was forked. Cleanflight Configurator was originally a fork of Baseflight Configurator with support for Cleanflight instead of Baseflight.
Stefan Kolla/cTn-dev - primary author and maintainer of Baseflight Configurator from which Cleanflight was forked.
