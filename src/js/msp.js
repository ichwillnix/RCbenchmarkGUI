'use strict';

// MSP_codes needs to be re-integrated inside MSP object
var MSP_codes = {
    MSP_DEBUG:                  0,
    MSP_FIRMWARE:               1,
    MSP_BOARD:                  2,
    MSP_POLL:                   3,
    MSP_GETOHM:                 4,
};

var MSP = {
    state:                      0,
    debug_string:               [],
    debug_string_13_detect:     0,
    ready_detected:             0,
    code:                       0,
    message_length_expected:    0,
    message_length_received:    0,
    message_buffer:             null,
    message_buffer_uint8_view:  null,
    message_checksum:           0,
    tempProbesHtml:             1,
    callbacks:                  [],
    packet_error:               0,

    supportedBaudRates: [ // 0 based index.
        'AUTO',
        '9600',
        '19200',
        '38400',
        '57600',
        '115200',
        '230400',
        '250000',
    ],

    read: function (readInfo) {
        var data = new Uint8Array(readInfo.data);

        for (var i = 0; i < data.length; i++) {
            switch (this.state) {
                case 0: // sync char 1
                    if (data[i] == 36) { // $
                        this.state++;
                    } else {
                        this.debug_string_fct(data[i]);
                    }
                    break;
                case 1: // sync char 2
                    if (data[i] == 82) { // R
                        this.state++;
                    } else { // restart and try again
                        this.state = 0;
                        this.debug_string_fct(data[i]);
                    }
                    break;
                case 2: // direction (should be >)
                    if (data[i] == 62) { // >
                        this.state++;
                    } else { // restart and try again
                        this.state = 0;
                        this.debug_string_fct(data[i]);
                    }
                    break;
                case 3:
                    this.message_length_expected = data[i];

                    this.message_checksum = data[i];

                    // setup arraybuffer
                    this.message_buffer = new ArrayBuffer(this.message_length_expected);
                    this.message_buffer_uint8_view = new Uint8Array(this.message_buffer);

                    this.state++;
                    break;
                case 4:
                    this.code = data[i];
                    this.message_checksum ^= data[i];

                    if (this.message_length_expected > 0) {
                        // process payload
                        this.state++;
                    } else {
                        // no payload
                        this.state += 2;
                    }
                    break;
                case 5: // payload
                    this.message_buffer_uint8_view[this.message_length_received] = data[i];
                    this.message_checksum ^= data[i];
                    this.message_length_received++;

                    if (this.message_length_received >= this.message_length_expected) {
                        this.state++;
                    }
                    break;
                case 6:
                    if (this.message_checksum == data[i]) {
                        // message received, process
                        //console.log('Received code: ' + this.code + ' - crc passed');
                        this.process_data(this.code, this.message_buffer, this.message_length_expected);
                    } else {
                        console.log('Received code: ' + this.code + ' - crc failed');

                        this.packet_error++;
                        $('span.packet-error').html(this.packet_error);
                    }

                    // Reset variables
                    this.message_length_received = 0;
                    this.state = 0;
                    break;

                default:
                    console.log('Unknown state detected: ' + this.state);
            }
        }
    },

    //Displays anything that is not a protocol as debug strings.
    //(allows using Serial.println() anywhere in the firmware...)
    debug_string_fct: function (char) {  
        if(char == 13){
            this.debug_string_13_detect = 1;
        }else{
            if(this.debug_string_13_detect && char == 10){
                GUI.log('Firmware message: <strong>' + this.debug_string + '</strong>');
                if(this.debug_string === "Ready") {
                    MSP.ready_detected = true;
                    CONFIG.readyCallback();  //board is ready and initialized, we can now start polling data
                } 
                this.debug_string = [];
            }else{
                this.debug_string += String.fromCharCode(char);
            }
            this.debug_string_13_detect = 0;
        }
    },

    //Processes the received serial data
    process_data: function (code, message_buffer, message_length) {
        var data = new DataView(message_buffer, 0); // DataView (allowing us to view arrayBuffer as struct/union)

        switch (code) {
            case MSP_codes.MSP_POLL:
                var index = 0;
                var acc_scale = 512;
                  
                SENSOR_DATA.ESCvoltage = data.getFloat32(index, 1); index+=4; //in V
                SENSOR_DATA.ESCcurrent = data.getFloat32(index, 1); index+=4; //in A
                SENSOR_DATA.power = data.getFloat32(index,1); index+=4; //in W
                SENSOR_DATA.loadCellThrust = data.getFloat32(index, 1); index+=4; //V
                SENSOR_DATA.loadCellLeft = data.getFloat32(index, 1); index+=4; //V
                SENSOR_DATA.eRPM_HZ = data.getFloat32(index, 1); index+=4; //V
                SENSOR_DATA.oRPM_HZ = data.getFloat32(index, 1); index+=4; //V
                SENSOR_DATA.temperature[0].value = data.getFloat32(index,1); index+=4; //in degC
                SENSOR_DATA.temperature[1].value = data.getFloat32(index,1); index+=4; //in degC
                SENSOR_DATA.temperature[2].value = data.getFloat32(index,1); index+=4; //in degC
                SENSOR_DATA.basicDataFlag = data.getUint8(index, 1); index+=1;
                if(SENSOR_DATA.ESCvoltage<0) SENSOR_DATA.ESCvoltage = 0;     
                SENSOR_DATA.accelerometer[0] = data.getInt16(index, 1) / acc_scale; index+=2;
                SENSOR_DATA.accelerometer[1] = data.getInt16(index, 1) / acc_scale; index+=2;
                SENSOR_DATA.accelerometer[2] = data.getInt16(index, 1) / acc_scale; index+=2;
                SENSOR_DATA.vibration = data.getInt16(index, 1) / acc_scale; index+=2;
                SENSOR_DATA.rawPressureP = data.getInt16(index, 1); index+=2;
                SENSOR_DATA.rawPressureT = data.getInt16(index, 1); index+=2;
                SENSOR_DATA.loadCellRight = data.getFloat32(index, 1); index+=4;
                SENSOR_DATA.proDataFlag = data.getUint8(index, 1); index+=1;

                //Separating bits for flags
                FIRMWARE_FLAGS.oRPMFlag = SENSOR_DATA.basicDataFlag&(1<<6);
                FIRMWARE_FLAGS.eRPMFlag = SENSOR_DATA.basicDataFlag&(1<<5);
                FIRMWARE_FLAGS.loadCellLeftFlag = SENSOR_DATA.basicDataFlag&(1<<4);
                FIRMWARE_FLAGS.loadCellThrustFlag = SENSOR_DATA.basicDataFlag&(1<<3);
                FIRMWARE_FLAGS.power = SENSOR_DATA.basicDataFlag&(1<<2);
                FIRMWARE_FLAGS.escCurrentFlag = SENSOR_DATA.basicDataFlag&(1<<1);
                FIRMWARE_FLAGS.escVoltageFlag = SENSOR_DATA.basicDataFlag&(1<<0);
                FIRMWARE_FLAGS.temperatureProbes = SENSOR_DATA.basicDataFlag&(1<<7);
                FIRMWARE_FLAGS.loadCellRightFlag = SENSOR_DATA.proDataFlag&(1<<1);
                FIRMWARE_FLAGS.accelerometerFlag = SENSOR_DATA.proDataFlag&(1<<0);

                //Process RPM readings
                if(FIRMWARE_FLAGS.eRPMFlag) DATA.processElectricalRPM(SENSOR_DATA.eRPM_HZ);
                if(FIRMWARE_FLAGS.oRPMFlag) DATA.processOpticalRPM(SENSOR_DATA.oRPM_HZ);

                //Apply Low Pass Filter
                if(FIRMWARE_FLAGS.loadCellLeftFlag) LPFLeft.update(SENSOR_DATA.loadCellLeft);
                if(FIRMWARE_FLAGS.loadCellRightFlag) LPFRight.update(SENSOR_DATA.loadCellRight);
                if(FIRMWARE_FLAGS.loadCellThrustFlag) LPFThrust.update(SENSOR_DATA.loadCellThrust);
                if(FIRMWARE_FLAGS.power) LPFpower.update(SENSOR_DATA.power);
                if(FIRMWARE_FLAGS.escCurrentFlag) LPFescCurrent.update(SENSOR_DATA.ESCcurrent);
                if(FIRMWARE_FLAGS.escCurrentFlag) LPFescCurrentBurst.update(SENSOR_DATA.ESCcurrent);
                if(FIRMWARE_FLAGS.escVoltageFlag) LPFescVoltage.update(SENSOR_DATA.ESCvoltage);
                LPFpressure.update(SENSOR_DATA.rawPressureP);
                LPFpressureT.update(SENSOR_DATA.rawPressureT);
                
                if(FIRMWARE_FLAGS.temperatureProbes){
                    if(MSP.tempProbesHtml){
                        htmlTempProbes();
                        MSP.tempProbesHtml = 0;
                    }
                    refreshTempProbes();
                    //console.log("Temps: " + SENSOR_DATA.temperature[0] + "C, " + SENSOR_DATA.temperature[1] + "C, " + SENSOR_DATA.temperature[2] + "C");
                }

                break;
            case MSP_codes.MSP_DEBUG:
                for (var i = 0; i < 4; i++)
                    SENSOR_DATA.debug[i] = data.getInt16((2 * i), 1);
                for (var i = 0; i < 4; i++)
                    SENSOR_DATA.pinState[i] = data.getUint8(8 + i, 1);
                break;
            case MSP_codes.MSP_FIRMWARE:
                var offset = 0;
                CONFIG.mspProtocolVersion = data.getUint8(offset++);
                CONFIG.firmwareVersion = data.getUint8(offset++) + '.' + data.getUint8(offset++);
                break;
            case MSP_codes.MSP_BOARD:
                var offset = 0;
                var boardVersion = data.getUint8(offset++);
                CONFIG.boardVersion = "Unknown board version";
                if(boardVersion===1) CONFIG.boardVersion = "Series 1580";
                if(boardVersion===2) CONFIG.boardVersion = "Series 1520";
                if(boardVersion===3) CONFIG.boardVersion = "Series 1580";
                showBoardSpecific();
                
                //Get the board unique ID
                var id = []; offset++; //skip family code
                for (var i = 0; i < 7; i++){
                    id += ("00" + data.getUint8(offset++).toString(16)).slice(-2);
                }
                CONFIG.boardId = id.toUpperCase(); 
                
                
                CONFIG.pSensorAvailable = data.getUint8(offset++);
                CONFIG.tempProbesQty = data.getUint8(offset++); 
                console.log(CONFIG.tempProbesQty + " temperature probes detected.");
                
                //Get the temperature probe ID's
                for(var probe = 0; probe < 3; probe++){
                    id = []; offset++; //skip family code
                    var dispid = 0; //A short version for display
                    for (var i = 0; i < 7; i++){
                        var unit8val = data.getUint8(offset++);
                        id += ("00" + unit8val.toString(16)).slice(-2);
                        dispid = 3*dispid + unit8val; //number here controls the lenght of the short ID. short has higher risk of collision. For full resolution use 256.
                    }
                    id = id.toUpperCase();
                    dispid = dispid.toString(32).toUpperCase();
                    console.log("Probe ID: " + id + "(" + dispid + ")");
                    SENSOR_DATA.temperature[probe].id = dispid;
                }
                MSP.tempProbesHtml = true;
                break;
            case MSP_codes.MSP_GETOHM:
                var index = 0;
                SENSOR_DATA.ohmStatus = data.getUint8(index++);
                SENSOR_DATA.ohmReading = data.getFloat32(index, 1); index +=4;
                break;
            default:
                console.log('Unknown code detected: ' + code);
        }

        // trigger callbacks, cleanup/remove callback after trigger
        for (var i = this.callbacks.length - 1; i >= 0; i--) { // itterating in reverse because we use .splice which modifies array length
            if (this.callbacks[i].code == code) {
                // save callback reference
                var callback = this.callbacks[i].callback;

                // remove timeout
                clearInterval(this.callbacks[i].timer);

                // remove object from array
                this.callbacks.splice(i, 1);

                // fire callback
                if (callback) callback({'command': code, 'data': data, 'length': message_length});
            }
        }
    },

    //Sends the polled data
    send_poll: function (callback) {
        var data = new ArrayBuffer(8);
        var dataview = new DataView(data); // DataView (allowing us to view arrayBuffer as struct/union)
        var index = 0;
        dataview.setUint16(index,OUTPUT_DATA.ESC_PWM,1); index+=2;
        dataview.setUint16(index,OUTPUT_DATA.Servo_PWM[0],1); index+=2; 
        dataview.setUint16(index,OUTPUT_DATA.Servo_PWM[1],1); index+=2; 
        dataview.setUint16(index,OUTPUT_DATA.Servo_PWM[2],1); index+=2;

        // set to zero the inactive outputs
        for(var i=0; i<4; i++){
            if(!OUTPUT_DATA.active[i]){
                dataview.setUint16(2*i,0,1);
            }
        }
               
        data = new Uint8Array(data);
        
        if(CONFIG.developper_mode){
            MSP.send_message(MSP_codes.MSP_DEBUG, false, false, false); //poll the debug info too
        }  
        MSP.send_message(MSP_codes.MSP_POLL, data, false, callback);
    },

    //Send a message to the serial port
    send_message: function (code, data, callback_sent, callback_msp) {
        var bufferOut,
            bufView;

        // always reserve 6 bytes for protocol overhead !
        if (data) {
            var size = data.length + 6,
                checksum = 0;

            bufferOut = new ArrayBuffer(size);
            bufView = new Uint8Array(bufferOut);

            bufView[0] = 36; // $
            bufView[1] = 82; // R
            bufView[2] = 60; // <
            bufView[3] = data.length;
            bufView[4] = code;

            checksum = bufView[3] ^ bufView[4];

            for (var i = 0; i < data.length; i++) {
                bufView[i + 5] = data[i];

                checksum ^= bufView[i + 5];
            }

            bufView[5 + data.length] = checksum;
        } else {
            bufferOut = new ArrayBuffer(6);
            bufView = new Uint8Array(bufferOut);

            bufView[0] = 36; // $
            bufView[1] = 82; // R
            bufView[2] = 60; // <
            bufView[3] = 0; // data length
            bufView[4] = code; // code
            bufView[5] = bufView[3] ^ bufView[4]; // checksum
        }

        //console.log('Sending code: ' + code);

        // dev version 0.57 code below got recently changed due to the fact that queueing same MSP codes was unsupported
        // and was causing trouble while backup/restoring configurations
        // watch out if the recent change create any inconsistencies and then adjust accordingly
        var obj = {'code': code, 'requestBuffer': bufferOut, 'callback': (callback_msp) ? callback_msp : false, 'timer': false};

        var requestExists = false;
        for (var i = 0; i < MSP.callbacks.length; i++) {
            if (MSP.callbacks[i].code == code) {
                // request already exist, we will just attach
                requestExists = true;
                break;
            }
        }

        if (!requestExists) {
            obj.timer = setInterval(function () {
                console.log('MSP data request timed-out: ' + code);

                //Restart the polling if it is the poll message,
                //otherwise just try again until solved
                if(code == MSP.POLL){
                    repeated_sensor_poll();
                }else{
                    serial.send(bufferOut, false);
                }
                
            }, 1000); // we should be able to define timeout in the future
        }

        MSP.callbacks.push(obj);

        // always send messages with data payload (even when there is a message already in the queue)
        if (data || !requestExists) {
            serial.send(bufferOut, function (sendInfo) {
                if (sendInfo.bytesSent == bufferOut.length) {
                    if (callback_sent) callback_sent();
                }
            });
        }

        return true;
    },
    callbacks_cleanup: function () {
        for (var i = 0; i < this.callbacks.length; i++) {
            clearInterval(this.callbacks[i].timer);
        }

        this.callbacks = [];
    },
    disconnect_cleanup: function () {
        this.state = 0; // reset packet state for "clean" initial entry (this is only required if user hot-disconnects)
        this.packet_error = 0; // reset CRC packet error counter for next session

        this.callbacks_cleanup();
    }
};