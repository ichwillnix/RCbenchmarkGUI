'use strict';

// rcb api instantiation.

var GUI_log = '';
var DATA_control = function(){
    
}

var RPM_filter_arr = [];
var RPM_time_arr = [];
var RPM_filter_okCount = 0;
var previousValid = false;

// Filter the rpm frequency and convert to RPM
DATA_control.prototype.processElectricalRPM = function(RPM_HZ){
    //If data is valid, pass through LPF
    if(RPM_HZ != 0){
        //Convert to RPM
        var RPMval = 2*60*RPM_HZ/CONFIG.numberOfMotorPoles.value;
        if(previousValid == false){
            //Force lpf to take the latest value
            LPFmotorRPM.forceValue(RPMval);
            previousValid = true;
        }else{
            //Apply the lpf
            LPFmotorRPM.update(RPMval);
        }
    }else{
        LPFmotorRPM.forceValue(0);
        previousValid = false;
    } 
}

DATA_control.prototype.processOpticalRPM = function(RPM_HZ){
    //If data is valid, pass through LPF
        if (RPM_HZ != 0){
            var RPMval = 60*RPM_HZ/CONFIG.numberOfOpticalTape.value;
            LPFmotorOpticalRPM.update(RPMval);
        }    
        else{
            LPFmotorOpticalRPM.forceValue(0);
        }
} 


// Tare the right and left load cell
DATA_control.prototype.tareLoadCells = function(callback){
	LPFLeft.forceNextValue();
   LPFRight.forceNextValue();
	LPFThrust.forceNextValue();
	
	// Wait for LPF to stabilize
    setTimeout(function(){
      // Set tare value
      TARE_OFFSET.loadCellLeftResetValue = -DATA.getLoadCellLeft();
      TARE_OFFSET.loadCellRightResetValue = -DATA.getLoadCellRight();
		TARE_OFFSET.loadCellThrustResetValue = -DATA.getLoadCellThrust();
      console.log("Tare Cells complete.");    
      chrome.storage.local.set({'TARE_OFFSET': TARE_OFFSET});   
      if (callback) callback();
    }, TARE_OFFSET.tareTime);
}; 

//Raw load cell value in mV
DATA_control.prototype.getLoadCellRight = function(){
    return LPFRight.getValue()*1000;
}

//Raw load cell value in mV
DATA_control.prototype.getLoadCellLeft = function(){
    return LPFLeft.getValue()*1000;
}

//Raw load cell value in mV
DATA_control.prototype.getLoadCellThrust = function(){
    return LPFThrust.getValue()*1000;
}

//Returns the thrust in kg
DATA_control.prototype.getThrust = function(){
    var calibratedThrust = (DATA.getLoadCellThrust() + TARE_OFFSET.loadCellThrustResetValue)*LOAD_CELLS_CALIBRATION.calibrationFactorThrust;
    var thrust = calibratedThrust*LOAD_CELLS_CALIBRATION.thrustNominal/LOAD_CELLS_CALIBRATION.fullScaleVoltage;
    if(CONFIG.reverseThrust.value){
        thrust = -thrust;
    }
    return thrust;
}

//Returns the torque in Nm
DATA_control.prototype.getTorque = function(){
    var calibratedLoadCellRight = (DATA.getLoadCellRight() + TARE_OFFSET.loadCellRightResetValue)*LOAD_CELLS_CALIBRATION.calibrationFactorRight;
    var calibratedLoadCellLeft = (DATA.getLoadCellLeft() + TARE_OFFSET.loadCellLeftResetValue)*LOAD_CELLS_CALIBRATION.calibrationFactorLeft;
    var difference = (LOAD_CELLS_CALIBRATION.calibrationFactorHingeRight*calibratedLoadCellRight - LOAD_CELLS_CALIBRATION.calibrationFactorHingeLeft*calibratedLoadCellLeft) * CONFIG.gravityConstant * LOAD_CELLS_CALIBRATION.leftRightNominal/LOAD_CELLS_CALIBRATION.fullScaleVoltage; //in Newtons
    var torque = difference*LOAD_CELLS_CALIBRATION.hingeDistance/2; //Torque in Nm. Divided by two because two load cells.
    if(CONFIG.reverseTorque.value){
        torque = -torque;
    }
    return torque;
}

//Returns measured weight in kg
DATA_control.prototype.getWeight = function(){
    var calibratedLoadCellRight = (DATA.getLoadCellRight() + TARE_OFFSET.loadCellRightResetValue)*LOAD_CELLS_CALIBRATION.calibrationFactorRight;
    var calibratedLoadCellLeft = (DATA.getLoadCellLeft() + TARE_OFFSET.loadCellLeftResetValue)*LOAD_CELLS_CALIBRATION.calibrationFactorLeft;
    var calibratedWeight = calibratedLoadCellRight + calibratedLoadCellLeft;
    return calibratedWeight*LOAD_CELLS_CALIBRATION.leftRightNominal/LOAD_CELLS_CALIBRATION.fullScaleVoltage;
}

DATA_control.prototype.getESCVoltage = function(){
    return LPFescVoltage.getValue();
}

DATA_control.prototype.getESCCurrent = function(){
    var val = LPFescCurrent.getValue();
    if(Math.abs(val) < 0.015) val = 0;
    return val;
}

DATA_control.prototype.getESCCurrentBurst = function(){
    var val = LPFescCurrentBurst.getValue();
    if(Math.abs(val) < 0.01) val = 0;
    return val;
}

DATA_control.prototype.getElectricalRPM = function () {
    var rpm = 0
    if (CONFIG.electricalRPMActive.value) {
        rpm = LPFmotorRPM.getValue();
    }
    return rpm;
}
DATA_control.prototype.getOpticalRPM = function () {
    var rpm = 0;
    if (CONFIG.opticalRPMActive.value) {
        rpm = LPFmotorOpticalRPM.getValue();
    }
    return rpm;
}
DATA_control.prototype.getRPM = function(){
    var rpm;
    switch (CONFIG.mainRPMsensor.value) {
        case "optical":
            rpm = DATA.getOpticalRPM();
            break;
        case "electrical":
            rpm = DATA.getElectricalRPM();
            break;
        default:
            throw "Invalid RPM sensor type"
    }
    return rpm;
}

DATA_control.prototype.getTemperatures = function(){
    var result = [];
    for (var i=0;i<CONFIG.tempProbesQty;i++){
        var id = SENSOR_DATA.temperature[i].id;
        var name = getTempProbeLocation(id);
        if(name === "") name = id;
        var value = {
            "value": SENSOR_DATA.temperature[i].value,
            "id": id,
            "label": name
        }
        result.push(value);
    }
    
    return result;
}

DATA_control.prototype.getAccelerometer = function(number){
    var acc = SENSOR_DATA.accelerometer[number];
    
    return acc;
}

DATA_control.prototype.getElectricalPower = function(){
    var elec_power = LPFpower.getValue();
    return elec_power;
}

DATA_control.prototype.getMechanicalPower = function(){
    var mech_power = Math.abs(DATA.getTorque()*DATA.getRPM()*UNITS.conversions.rpm.rads);
    return mech_power;
}

DATA_control.prototype.getMotorEfficiency = function(){
  var efficiency;
  if (DATA.getElectricalPower() == 0) {
    efficiency = 0;
  }
  else {
    efficiency = DATA.getMechanicalPower()/DATA.getElectricalPower()*100;

  }  
    return efficiency;
}
DATA_control.prototype.getPropMechEfficiency = function () {
    var efficiency;
    if (DATA.getMechanicalPower() == 0) {
        efficiency = 0;
    } else {
        efficiency = DATA.getThrust() / DATA.getMechanicalPower();
        //Efficiency should not be over 100g/watts
        if (efficiency > 0.1) {
            efficiency = 0;
        }

    }
    return Math.abs(efficiency);
}

DATA_control.prototype.getPropElecEfficiency = function () {
    var efficiency;
    if ((DATA.getElectricalPower() == 0) || (DATA.getRPM() == 0)) {
        efficiency = 0;
    } else {
        efficiency = DATA.getThrust() / DATA.getElectricalPower();
        //Efficiency should not be over 100g/watts
        if (efficiency > 0.1) {
            efficiency = 0;
        }
    }
    return Math.abs(efficiency);
}

DATA_control.prototype.isPressureSensorAvailable = function () {
    if (CONFIG.pSensorAvailable) {
        return true;
    }
    else {
        return false;
    }
}

DATA_control.prototype.getPressureT = function () {
    var temp;
    // Output in degree C. See datasheet p.4
    temp = LPFpressureT.getValue() * (200) / 2047 -50;
    return temp;
}

DATA_control.prototype.getPressureRaw = function () {
    // Units are Pa
    var pressure;
    
    var Pmin = -1 * 6894.76; // Min pressure in Pa
    var Pmax = 1 * 6894.76; // Min pressure in Pa
    // Type A Sensor Datasheet p.4
    pressure = (LPFpressure.getValue() - 0.1 * 16383) * (Pmax - Pmin) / (0.8 * 16383) + Pmin;
    
    
    // If the sensor is not connected
    if (SENSOR_DATA.rawPressureP == 0) {
        pressure = 0;
    }
    
    return pressure;
}

DATA_control.prototype.getPressure = function () {
    // Units are Pa
    var pressure;
    pressure = CONFIG.pressureCal.value * (DATA.getPressureRaw() + CONFIG.pressureTare.value);
    return pressure;
}

DATA_control.prototype.tarePressure = function tarePressure(callback){
	// The function will not execute is it is already doing something.
    if (DATA.taringPressure === false || DATA.taringPressure == null) {    
        DATA.sampleInit = 50;
        DATA.sample = DATA.sampleInit;
        DATA.sum = 0;
        DATA.taringPressure = true;
        
        function averageSamples(callback) {
            DATA.sum =  DATA.sum + DATA.getPressureRaw();
            DATA.sample = DATA.sample - 1;
            console.log(DATA.getPressureRaw());
            if (DATA.sample === 0) {
                changeMemory(CONFIG.pressureTare, -DATA.sum / DATA.sampleInit);
                DATA.taringPressure = false;
                console.log("Tare Aispeed complete."); 
                if (callback) callback();
                return 0;
            }
            else {
                setTimeout(function(){
                    averageSamples(callback);
                }, 100);
            }
        }
        averageSamples(callback);
    }
}; 


DATA_control.prototype.getAirSpeed = function () {
    var airspeed;
    // Ambiant pressure in Pa
    var pAtm = CONFIG.atmPressure.value;
    // In kelvin
    var temperature = DATA.getPressureT() + 273.15;
    // Air density;
    var rho = pAtm / temperature / 287.058;
    airspeed = Math.sqrt(Math.abs(2*(DATA.getPressure())/ rho));
    return airspeed; //airspeed;
}

DATA_control.prototype.getForwardFlightEfficiency = function () {
    if (DATA.getElectricalPower() == 0) {
        return 0;
    }
    else {
        // Forward power / electrical power in percentage.
        var out = Math.abs(DATA.getThrust() * 9.81 * DATA.getAirSpeed() / DATA.getElectricalPower() * 100);
        return out;
    }
    
}

var DATA = new DATA_control();


