'use strict';

$(document).ready(function () {
	$("#connect-progress").hide();
	$("div#connectionInfo").html(chrome.i18n.getMessage('notConnected'));
	$('div#port-picker a.connect').click(function () {
		if(!GUI.firmware_is_flashing) connection();
	});
	
    // auto-connect
    /*chrome.storage.local.get('auto_connect', function (result) {
        if (result.auto_connect === 'undefined' || result.auto_connect) {
            // default or enabled by user
            GUI.auto_connect = true;

            $('input.auto_connect').prop('checked', true);
            $('input.auto_connect, span.auto_connect').prop('title', chrome.i18n.getMessage('autoConnectEnabled'));

            $('select#baud').val(115200).prop('disabled', true);
        } else {
            // disabled by user
            GUI.auto_connect = false;

            $('input.auto_connect').prop('checked', false);
            $('input.auto_connect, span.auto_connect').prop('title', chrome.i18n.getMessage('autoConnectDisabled'));
        }

        // bind UI hook to auto-connect checkbos
        $('input.auto_connect').change(function () {
            GUI.auto_connect = $(this).is(':checked');

            // update title/tooltip
            if (GUI.auto_connect) {
                $('input.auto_connect, span.auto_connect').prop('title', chrome.i18n.getMessage('autoConnectEnabled'));
                $('select#baud').val(115200).prop('disabled', true);

            } else {
                $('input.auto_connect, span.auto_connect').prop('title', chrome.i18n.getMessage('autoConnectDisabled'));

                if (!GUI.connected_to && !GUI.connecting_to) $('select#baud').prop('disabled', false);
            }

            chrome.storage.local.set({'auto_connect': GUI.auto_connect});
        });
        googleAnalytics.sendEvent('Autoconnect', 'Using', GUI.auto_connect);
    });*/

    PortHandler.initialize();
    PortUsage.initialize();
});
	
function connection() {
	if (GUI.connect_lock != true) { // GUI control overrides the user control
		var clicks = $(this).data('clicks'),
			selected_port = String($('div#port-picker #port').val()),
			selected_baud = parseInt($('div#port-picker #baud').val());

		if (selected_port != '0' && selected_port != 'DFU') {
			var isConnected = GUI.connected_to && (GUI.connected_to.valueOf().trim() != "");
			if (!isConnected) {
				console.log('Connecting to: ' + selected_port);
				GUI.connecting_to = selected_port;

				// lock port select & baud while we are connecting / connected
				$("#connect-progress").show();
				$("div#connectionInfo").hide();
				$('div#port-picker #port, div#port-picker #baud, div#port-picker #delay').prop('disabled', true);
				$('div#port-picker a.connect').text(chrome.i18n.getMessage('connecting'));
				MSP.ready_detected = false;
				serial.connect(selected_port, {bitrate: selected_baud}, onOpen);
			} else {
				GUI.timeout_kill_all();
				GUI.interval_kill_all();
				GUI.tab_switch_cleanup();
				GUI.tab_switch_in_progress = false;

				serial.disconnect(onClosed);

				var wasConnected = CONFIGURATOR.connectionValid;
				
				GUI.connected_to = false;
				CONFIGURATOR.connectionValid = false;
				GUI.allowedTabs = GUI.defaultAllowedTabsWhenDisconnected.slice();
				MSP.disconnect_cleanup();
				PortUsage.reset();

				// Reset various UI elements
				$('span.i2c-error').text(0);
				$('span.cycle-time').text(0);
				
				//hide left pane elements
				$('#left-pane #info p#warning, div#data, #left-pane #info p.calibrationThrust, #left-pane #info p.calibrationTorque, #left-pane #info p.oldCalibrationThrust, #left-pane #info p.oldCalibrationTorque').hide();
				
				// unlock port select & baud
				$('div#port-picker #port').prop('disabled', false);
				if (!GUI.auto_connect) $('div#port-picker #baud').prop('disabled', false);

				// reset connect / disconnect button
				$("#connect-progress").hide();
				$("div#connectionInfo").html(chrome.i18n.getMessage('notConnected'));
				$("div#connectionInfo").show();
				$('div#port-picker a.connect').text(chrome.i18n.getMessage('connect'));
				$('div#port-picker a.connect').removeClass('active');
				$('#polesSetting').hide();

				// reset active sensor indicators
				sensor_status(0);

				if (wasConnected) {
					if(!GUI.firmware_is_flashing) $('#tabs .tab_landing a').click();
				}
			}

			$(this).data("clicks", !clicks);
		}
	}
};

function onOpen(openInfo) {
    var wasFlashing = GUI.firmware_is_flashing;
	GUI.firmware_is_flashing = false;
    if (openInfo) {
		
        // update connected_to
        GUI.connected_to = GUI.connecting_to;

        // reset connecting_to
        GUI.connecting_to = false;

        GUI.log(chrome.i18n.getMessage('serialPortOpened', [openInfo.connectionId]));

        setTimeout(function(){
        	if(!MSP.ready_detected) CONFIG.readyCallback(); //In case the board reset pulse didn't work
        }, 3000);

        // save selected port with chrome.storage if the port differs
        chrome.storage.local.get('last_used_port', function (result) {
            if (result.last_used_port) {
                if (result.last_used_port != GUI.connected_to) {
                    // last used port doesn't match the one found in local db, we will store the new one
                    chrome.storage.local.set({'last_used_port': GUI.connected_to});
                }
            } else {
                // variable isn't stored yet, saving
                chrome.storage.local.set({'last_used_port': GUI.connected_to});
            }
        });

        serial.onReceive.addListener(read_serial);

        //Waits for the board to send its "Ready" string.
        CONFIG.readyCallback = function() {
        	MSP.ready_detected = true;
        	
            // disconnect after 2.5 seconds with error if we don't get IDENT data
            GUI.timeout_add('connecting', function () {
                if (!CONFIGURATOR.connectionValid) {
                    GUI.log(chrome.i18n.getMessage('noConfigurationReceived'));
                    $('div#port-picker a.connect').click(); // disconnect
                    $("div#connectionInfo").html(chrome.i18n.getMessage('notConnectedTimeout'));
                }
            }, 2500);
            
            // request configuration data
            MSP.send_message(MSP_codes.MSP_FIRMWARE, false, false, function () {
                GUI.log(chrome.i18n.getMessage('firmwareVersion', [CONFIG.firmwareVersion]));
                googleAnalytics.sendEvent('Firmware', 'Using', CONFIG.firmwareVersion);
				
				//Firmware labels
				setFirmwareLabels();
                
                if (CONFIG.firmwareVersion === CONFIGURATOR.firmwareVersionAccepted) {
                    MSP.send_message(MSP_codes.MSP_BOARD, false, false, function () {

                        var boardLog = chrome.i18n.getMessage('boardInfoReceived', [CONFIG.boardVersion]);
                        if(CONFIG.boardVersion === "Series 1580"){
                            boardLog += chrome.i18n.getMessage('boardInfoReceivedId', [CONFIG.boardId]);
                        }
                        GUI.log(boardLog);
                        googleAnalytics.sendEvent('Board', 'Using', CONFIG.boardVersion);
						var plotsWrappers = '<label><b>Hardware:</b></br>Connected!</br>';
						plotsWrappers += CONFIG.boardVersion + ', ';
						plotsWrappers += 'firmware v' + CONFIG.firmwareVersion;
						plotsWrappers += '<\label></br></br>';
						$('#data-pane #hardware').html(plotsWrappers); 
						$('#polesSetting').show();
	
                        // Activate allowed tabs and setup GUI as connected
                        CONFIGURATOR.connectionValid = true;
                        GUI.allowedTabs = GUI.defaultAllowedTabsWhenConnected.slice();
                        onConnect();

                        OUTPUT_DATA.active[0] = 0;
                        OUTPUT_DATA.ESC_PWM = 1000;

						handleCalibration();
						
						// Select what tab is shown when connected
                        TABS.sensors.initialize(); //starts the plots scrolling
				        repeated_sensor_poll(); // Automatically start sensors polling 
						
                    });
                } else {
                    GUI.log(chrome.i18n.getMessage('firmwareVersionNotSupported', [CONFIGURATOR.firmwareVersionAccepted]));
                    $('a.connect').click(); //disconnect
                    $('#tabs .tab_setup a').click();
                    setFirmwareLabels();
					$('#polesSetting').hide();
                }
            });
        }
    } else {
        console.log('Failed to open serial port');
        GUI.log(chrome.i18n.getMessage('serialPortOpenFail'));
		
		$('#polesSetting').hide();
        $('div#port-picker a.connect').text(chrome.i18n.getMessage('connect'));
        $('div#port-picker a.connect').removeClass('active');
				
        // unlock port select & baud
        $('div#port-picker #port, div#port-picker #baud, div#port-picker #delay').prop('disabled', false);

        // reset data
        $('div#port-picker a.connect').data("clicks", false);
    }
}

function display_text(){
	//Generate text to display (with limited update rate)
  	var currTimeMs = window.performance.now(); //time in ms
	var dt = (currTimeMs - display_text.lastTimestamp);
	if(dt>150 || isNaN(dt)){
		var plotsWrappers = '<dl>\n';
        var temperatures = DATA.getTemperatures();
		if(CONFIGURATOR.connectionValid){
			plotsWrappers += '<dd><b>Sensors:</b></dd>\n';
			plotsWrappers += '<dt class="voltage">Voltage: <value class="voltage"></value> V</dt>\n';
			plotsWrappers += '<dt class="current">Current: <value class="current"></value> A</dt>\n';
            plotsWrappers += '<dt class="power">Elec. Power: <value class="power"></value> W</dt>\n';
			plotsWrappers += '<dt class="thrust">Thrust: <value class="thrust"></value> ' + UNITS.text[UNITS.display['thrust']] + '</dt>\n';
			if(CONFIG.boardVersion === "Series 1580") plotsWrappers += '<dt class="torque">Torque: <value class="torque"></value> ' + UNITS.text[UNITS.display['torque']] + '</dt>\n';
			if(CONFIG.boardVersion === "Series 1580") plotsWrappers += '<dt class="weight">Weight: <value class="weight"></value> ' + UNITS.text[UNITS.display['weight']] + '</dt>\n';
            if(CONFIG.boardVersion === "Series 1580") plotsWrappers += '<dt class="vibration">Vibration: <value class="vibration"></value> g</dt>\n';
			if(CONFIG.electricalRPMActive.value){
                plotsWrappers += '<dt class="rpmElectrical">Motor Speed, ' + CONFIG.numberOfMotorPoles.value + ' poles: <value class="rpm"></value> ' + UNITS.text[UNITS.display['motorSpeed']] + '</dt>\n';
            }
            if(CONFIG.opticalRPMActive.value){
                var plural = "";
                if(CONFIG.numberOfOpticalTape.value>1) 
                    plural = "s";
                plotsWrappers += '<dt class="rpmOptical">Motor speed, ' + CONFIG.numberOfOpticalTape.value + ' tape' + plural + ': <value class="optical_rpm"></value> ' + UNITS.text[UNITS.display['motorSpeed']] + '</dt>\n';
            }
            if(CONFIG.boardVersion === "Series 1580"){
                for(var i=0; i<temperatures.length; i++){
                    var temp = temperatures[i];
                    var id = "temp" + temp.id;
                    var label = temp.label + " Temp: ";
                    var unit = UNITS.text[UNITS.display['temperature']];
                    plotsWrappers += '<dt class="' + id + '">' + label + '<value class="' + id + '"></value> ' + unit + '</dt>\n';
                };
                if (DATA.isPressureSensorAvailable()) {
                    plotsWrappers += '<dt class="airspeed">Aispeed: <value class="airspeed"></value> ' + UNITS.text[UNITS.display['speed']] + '</dt>\n';
                }
                
            }
			if(CONFIG.developper_mode) {
				plotsWrappers += '<dt class="usb">USB rate: <value class="usb"></value> Hz</dt>\n';
				if(CONFIG.boardVersion === "Series 1580")plotsWrappers += '<dt class="loadleft">Left load cell: <value class="loadleft"></value> mV</dt>\n';
				if(CONFIG.boardVersion === "Series 1580")plotsWrappers += '<dt class="loadright">Right load cell: <value class="loadright"></value> mV</dt>\n';
				plotsWrappers += '<dt class="loadthrust">Thrust load cell: <value class="loadthrust"></value> mV</dt>\n';
                plotsWrappers += '<dt class="currentBurst">Burst Current LPF: <value class="currentBurst"></value> A</dt>\n';
				plotsWrappers += '<dt class="pinPwm">Pin State ESC: <value class="pinPwm"></value></dt>\n';
                plotsWrappers += '<dt class="pinServo1">Pin State Servo1: <value class="pinServo1"></value></dt>\n';
                plotsWrappers += '<dt class="pinServo2">Pin State Servo2: <value class="pinServo2"></value></dt>\n';
                plotsWrappers += '<dt class="pinServo3">Pin State Servo3: <value class="pinServo3"></value></dt>\n';
                plotsWrappers += '<dt class="debug0">Debug 0: <value class="debug0"></value></dt>\n';
				plotsWrappers += '<dt class="debug1">Debug 1: <value class="debug1"></value></dt>\n';
				plotsWrappers += '<dt class="debug2">Debug 2: <value class="debug2"></value></dt>\n';
				plotsWrappers += '<dt class="debug3">Debug 3: <value class="debug3"></value></dt>\n';
			}
			plotsWrappers += '<dt class="none"><br></dt>\n';
			plotsWrappers += '<dt class="cutoff" style="display:none">***Safety cutoff activated!***</dt>\n';
		}else{
			plotsWrappers += '<dd><b></b></dd>\n';
		}
		plotsWrappers += '<\dl>';
		$('#data-pane #data').html(plotsWrappers);

		$('value.voltage').html(DATA.getESCVoltage().toFixed(2));
		$('value.current').html(DATA.getESCCurrent().toFixed(2));
        $('value.power').html(DATA.getElectricalPower().toFixed(0));
		$('value.thrust').html(UNITS.convertToDisplay('thrust', DATA.getThrust()).toFixed(3));
		if(CONFIG.boardVersion === "Series 1580")$('value.torque').html(UNITS.convertToDisplay('torque', DATA.getTorque()).toFixed(3));
		if(CONFIG.boardVersion === "Series 1580")$('value.weight').html(UNITS.convertToDisplay('weight', DATA.getWeight()).toFixed(3));
        if(CONFIG.boardVersion === "Series 1580")$('value.vibration').html(SENSOR_DATA.vibration.toFixed(1));
		$('value.rpm').html(Math.round(UNITS.convertToDisplay('motorSpeed', DATA.getElectricalRPM()).toFixed(3)));
        $('value.airspeed').html(UNITS.convertToDisplay('speed', DATA.getAirSpeed()).toFixed(2));
        $('value.optical_rpm').html(Math.round(UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM()).toFixed(3)));
        if(CONFIG.boardVersion === "Series 1580"){
            for(var i=0; i<temperatures.length; i++){    
                var temp = temperatures[i];
                var id = "temp" + temp.id;
                $('value.' + id).html(UNITS.convertToDisplay('temperature', temp.value).toFixed(1));
            };
        $('value.airpressure').html(UNITS.convertToDisplay('pressure', DATA.getPressure()).toFixed(2) +
                                   " " + UNITS.text[UNITS.display['pressure']]);
        }
		if(CONFIG.developper_mode){
			$('value.usb').html(SENSOR_DATA.updateRate.toFixed(0));	
			$('value.loadleft').html(DATA.getLoadCellLeft().toFixed(4));
			$('value.loadright').html(DATA.getLoadCellRight().toFixed(4));
			$('value.loadthrust').html(DATA.getLoadCellThrust().toFixed(4));
            $('value.currentBurst').html(DATA.getESCCurrentBurst().toFixed(2));
			$('value.pinPwm').html(SENSOR_DATA.pinState[0]);
            $('value.pinServo1').html(SENSOR_DATA.pinState[1]);
            $('value.pinServo2').html(SENSOR_DATA.pinState[2]);
            $('value.pinServo3').html(SENSOR_DATA.pinState[3]);
            $('value.debug0').html(SENSOR_DATA.debug[0]);
			$('value.debug1').html(SENSOR_DATA.debug[1]);
			$('value.debug2').html(SENSOR_DATA.debug[2]);
			$('value.debug3').html(SENSOR_DATA.debug[3]);
		}
		display_text.lastTimestamp = currTimeMs;
	}
}

function repeated_sensor_poll() {
	//Calculate the USB rate
  	var currTimeMs = window.performance.now(); //time in ms
	var dt = 0.001 * (currTimeMs - repeated_sensor_poll.lastTimestamp);
	SENSOR_DATA.updateRate = 1/dt; //frequency in Hz
  	repeated_sensor_poll.lastTimestamp = currTimeMs;

	display_text();
	safetyLimits();	
	update_plots();
	script_update();
	logSample.update();

	MSP.send_poll(repeated_sensor_poll);
} 

function updateLeftPanelView(show){
    if(show) $('div#data').show();
    $("#left-pane-tare-button a").on('click', function() {
        var sel = $(this);
        sel.text("Please wait...");
        googleAnalytics.sendEvent('Tare', 'Click');
        DATA.tareLoadCells(function(){
            if(CONFIG.boardVersion === "Series 1580"){
                sel.text(chrome.i18n.getMessage('tareLoadCells'));   
            }else{
                sel.text(chrome.i18n.getMessage('tareThrust')); 
            }
        });
    });
    $("#left-pane-tare-airspeed-button a").on('click', function() {
        var sel = $(this);
        sel.text("Please wait...");
        googleAnalytics.sendEvent('TareAirspeed', 'Click');
        DATA.tarePressure(function(){
            sel.text(chrome.i18n.getMessage('tareAirSpeed'));   
        });
    });
    if(show) {
        $("#left-pane-tare-button a").show();
        setTimeout( function () {
                if( DATA.isPressureSensorAvailable()) {
                    $("#left-pane-tare-airspeed-button a").show();
                }
            }, 300);
    }

}

function onConnect() {
    GUI.timeout_remove('connecting'); // kill connecting timer
    $("#connect-progress").hide();
    $("div#connectionInfo").hide();
    $('div#port-picker a.connect').text(chrome.i18n.getMessage('disconnect')).addClass('active');
	$('#tabs ul.mode-connected').removeClass('tab-disconnected');
	$('#plot').removeClass('plot-disconnected');
	googleAnalytics.sendEvent('Connect', 'Click');
    updateLeftPanelView(true);
}

function onClosed(result) {
    if (result) { // All went as expected
        GUI.log(chrome.i18n.getMessage('serialPortClosedOk'));
    } else { // Something went wrong
        GUI.log(chrome.i18n.getMessage('serialPortClosedFail'));
    }

	$('#data-pane #hardware').html(""); 
	$('#left-pane-tare-button a').off().hide(); //Hide the tare button in the left pane.
	$('#left-pane-tare-airspeed-button a').off().hide(); //Hide the tare button in the left pane.
    $('#tabs ul.mode-connected').addClass('tab-disconnected');
	$('#plot').addClass('plot-disconnected');
    
    var documentationButton = $('#button-documentation');
    documentationButton.hide();

    display_text();
    //$('.tare-buttons').hide();
}

function read_serial(info) {
    if (!CONFIGURATOR.cliActive) {
        MSP.read(info);
    } else if (CONFIGURATOR.cliActive) {
        TABS.cli.read(info);
    }
}

function sensor_status(sensors_detected) {
    // initialize variable (if it wasn't)
    if (!sensor_status.previous_sensors_detected) {
        sensor_status.previous_sensors_detected = 0;
    }

    // update UI (if necessary)
    if (sensor_status.previous_sensors_detected == sensors_detected) {
        return;
    }
    
    // set current value
    sensor_status.previous_sensors_detected = sensors_detected;

    var e_sensor_status = $('div#sensor-status');

    if (have_sensor(sensors_detected, 'acc')) {
        $('.accel', e_sensor_status).addClass('on');
    } else {
        $('.accel', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'gyro')) {
        $('.gyro', e_sensor_status).addClass('on');
    } else {
        $('.gyro', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'baro')) {
        $('.baro', e_sensor_status).addClass('on');
    } else {
        $('.baro', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'mag')) {
        $('.mag', e_sensor_status).addClass('on');
    } else {
        $('.mag', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'gps')) {
        $('.gps', e_sensor_status).addClass('on');
    } else {
        $('.gps', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'sonar')) {
        $('.sonar', e_sensor_status).addClass('on');
    } else {
        $('.sonar', e_sensor_status).removeClass('on');
    }
}

function have_sensor(sensors_detected, sensor_code) {
    switch(sensor_code) {
        case 'acc':
        case 'gyro':
            return bit_check(sensors_detected, 0);
        case 'baro':
            return bit_check(sensors_detected, 1);
        case 'mag':
            return bit_check(sensors_detected, 2);
        case 'gps':
            return bit_check(sensors_detected, 3);
        case 'sonar':
            return bit_check(sensors_detected, 4);
    }
    return false;
}

function highByte(num) {
    return num >> 8;
}

function lowByte(num) {
    return 0x00FF & num;
}

function specificByte(num, pos) {
    return 0x000000FF & (num >> (8 * pos));
}

function bit_check(num, bit) {
    return ((num >> bit) % 2 != 0);
}

function bit_set(num, bit) {
    return num | 1 << bit;
}

function bit_clear(num, bit) {
    return num & ~(1 << bit);
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}