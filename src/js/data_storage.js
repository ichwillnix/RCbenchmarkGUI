'use strict';

var CONFIGURATOR = {
    'releaseDate': 1438791056212, // type "new Date().getTime()" in the console. Will return the new number.
    'firmwareVersionAccepted': '1.15',
    'backupFileMinVersionAccepted': '0.70', // chrome.runtime.getManifest().version is stored as string, so does this one
    'connectionValid': false,
};

var CONFIG = {
    developper_mode: false,
    scripting_mode: false,
    safetyCutoffDisable: false,
    workingDirectory: undefined,
    firmwareVersion: 'unknown',
    gravityConstant: 9.80665,
    tempProbesQty: 0,
    pSensorAvailable: 0,
};

// See memorymanagement.js
// Do no add something to main.js anymore.
// Access with CONFIG.xxx.value
// Set with changeMemory(CONFIG.xxx, newValue);
CONFIG.electricalRPMActive = new storedVariable(true, 'electricalRPMactive');
CONFIG.opticalRPMActive = new storedVariable(true, 'opticalRPMactive');
CONFIG.numberOfMotorPoles = new storedVariable(12, 'pole_number');
CONFIG.numberOfOpticalTape = new storedVariable(1, 'optical_number');
CONFIG.mainRPMsensor = new storedVariable('electrical', 'mainRPMsensor');
CONFIG.temperatureProbes = new storedVariable({}, 'temperatureProbes');
CONFIG.ESCMin = new storedVariable(1000, 'ESCMin');
CONFIG.ESCMax = new storedVariable(2000, 'ESCMax');
CONFIG.ESCCutoff = new storedVariable(1000, 'ESCCutoff');
CONFIG.servo1Min = new storedVariable(1000, 'servo1Min');
CONFIG.servo1Max = new storedVariable(2000, 'servo1Max');
CONFIG.servo2Min = new storedVariable(1000, 'servo2Min');
CONFIG.servo2Max = new storedVariable(2000, 'servo2Max');
CONFIG.servo3Min = new storedVariable(1000, 'servo3Min');
CONFIG.servo3Max = new storedVariable(2000, 'servo3Max');
CONFIG.reverseThrust = new storedVariable(false, 'reverseThrust');
CONFIG.reverseTorque = new storedVariable(false, 'reverseTorque');
CONFIG.csvNotes = new storedVariable('', 'csvNotes');
CONFIG.pressureTare = new storedVariable(0, 'pressureTare');
CONFIG.pressureCal = new storedVariable(1, 'pressureCal');
CONFIG.atmPressure = new storedVariable(101325, 'atmPressure');

var SYSTEM_LIMITS = {
    voltageMin: 0,		    //[V]
    voltageMax: 35,			//[V]
    currentMin: -40,		//[A]
    currentMax: 40,			//[A]
    currentBurstMin: -50,		//[A]
    currentBurstMax: 50,	//[A]
    powerMin: 0,			//[W]
    powerMax: 1400,			//[W]
    rpmMin: 0,
    rpmMax: 0, //calculated upon init
    thrustMin: -5,			//[kg]
    thrustMax: 5,			//[kg]
    torqueMin: -2,			//[Nm]
    torqueMax: 2,			//[Nm]
    vibrationMin: 0,			//[g]
    vibrationMax: 8,			//[g]
    temperatureMin: -10,        //[C]
    temperatureMax: 120,         //[C]
    PWMTimeMin: 700,         // us Change in rcbApi.js too!
    PWMTimeMax: 2300         // us Change in rcbApi.js too!
};
var ORIGINAL_SYSTEM_LIMITS = $.extend(true, {}, SYSTEM_LIMITS);

var USER_LIMITS = {
    voltageMin: 0, //[V]
    voltageMax: 15, //[V]
    currentMin: 0, //[A]
    currentMax: 3, //[A]
    currentBurstMin: 0, //[A]
    currentBurstMax: 6, //[A]
    powerMin: 0, //[W]
    powerMax: 100, //[W]
    rpmMin: 0,
    rpmMax: 10000,
    thrustMin: -1, //[kg]
    thrustMax: 1, //[kg]
    torqueMin: -0.5, //[Nm]
    torqueMax: 0.5, //[Nm]
    vibrationMin: 0, //[g]
    vibrationMax: 2, //[g]
    temperatureMin: 10, //[C] default only for new probes
    temperatureMax: 60, //[C] default only for new probes
    temperaturesMin: {}, //[C] but matched with a specific probe id
    temperaturesMax: {} //[C] but matched with a specific probe id
};

var SENSOR_DATA = {
    accelerometer:  [0,0,0],
    vibration: 0,
    debug:          [0,0,0,0],
    pinState:          [0,0,0,0], //ESC, Servo1, Servo2, Servo3
    loadCellThrust: 0,
    loadCellLeft: 0,
    loadCellRight: 0,
    ESCvoltage: 0,
    ESCcurrent: 0,
    eRPM_HZ: 0,
    oRPM_HZ: 0,
    power:0,
    proDataFlag: 0,
    basicDataFlag: 0,
    updateRate: 0,
    lastOhm: NaN,
    rawPressureP: 0,
    rawPressureT: 0,
    temperature: [
        {id: "", value: 0},{id: "", value: 0},{id: "", value: 0}
    ],
};

var FIRMWARE_FLAGS = {
    escVoltageFlag: 0,
    escCurrentFlag: 0,
    power:0,
    loadCellThrustFlag: 0,
    loadCellLeftFlag: 0,
    loadCellRightFlag:0,
    eRPMFlag:0,
    oRPMFlag:0,
    accelerometerFlag:0,
    temperatureProbes:0
}

var LOAD_CELLS_CALIBRATION = {
    thrustNominal:  5, //kg (value of the load cell)
    leftRightNominal:  2, //kg (value of the load cell)
    fullScaleVoltage:   5, //mV (full scale sensitivity)         
    hingeDistance: 0.07492,   // [m] distance between the two hinge axes
    calibrationWeight: 0.2,	//[kg]
    calibrationWeightDistance: 0.13,       //[m]
    calibrationFactorLeft:   1.0,  // should be close to unity
    calibrationFactorRight:  1.0,            
    calibrationFactorThrust: 1.0,
    calibrationFactorHingeLeft:  1.0,
    calibrationFactorHingeRight:  1.0             
};

var TARE_OFFSET = {
   loadCellThrustResetValue: 0,   // [mV]
   loadCellRightResetValue: 0,    // [mV]
   loadCellLeftResetValue: 0,     // [mV]
   tareTime: 2500 //time to stabilize the lpf in ms.
};

//Filter cutoff frequency in Hz. Ie, everything above the value will be filtered out as noise.
//Write 0 to disable the filter
var FILTER_CONFIG = {
    lpfLeftAlpha: 1,
    lpfRightAlpha: 1,
    lpfThrustAlpha: 1,
    lpfCurrentAlpha: 1,
    lpfCurrentBurstAlpha: 1/20, //Acts like a slow fuse, allowing short duration burst current. UPDATE in burstcurrent.html if changed.
    lpfVoltageAlpha: 1,
    lpfMotorRpmAlpha: 20,
    lpfMotorOpticalRpmAlpha: 20,
    lpfPower: 1,
    lpfPressure : 0.2,
};

var OUTPUT_DATA = {
    ESC_PWM:        1000,
    Servo_PWM:      [1500,1500,1500],
    active:         [0,0,0,0]
};

var PROCESSED_DATA = {
    ESCcurrent:     0,
    motor_RPM: 0
};

var LPFThrust = new LowPassFilter(FILTER_CONFIG.lpfThrustAlpha);
var LPFRight = new LowPassFilter(FILTER_CONFIG.lpfRightAlpha);
var LPFLeft = new LowPassFilter(FILTER_CONFIG.lpfLeftAlpha);
var LPFescCurrent = new LowPassFilter(FILTER_CONFIG.lpfCurrentAlpha);
var LPFescCurrentBurst = new LowPassFilter(FILTER_CONFIG.lpfCurrentBurstAlpha);
var LPFescVoltage = new LowPassFilter(FILTER_CONFIG.lpfVoltageAlpha);
var LPFmotorRPM = new LowPassFilter(FILTER_CONFIG.lpfMotorRpmAlpha);
var LPFmotorOpticalRPM = new LowPassFilter(FILTER_CONFIG.lpfMotorOpticalRpmAlpha);
var LPFpower = new LowPassFilter(FILTER_CONFIG.lpfPower);
var LPFpressure = new LowPassFilter(FILTER_CONFIG.lpfPressure);
var LPFpressureT = new LowPassFilter(FILTER_CONFIG.lpfPressure);

var UNITS = {
   fullText:{  //how each unit will be printed in the dropdown box
		N: "Newton",
        Nm: "Newton meter",
		dyn: "Dyne",
		dynm: "Dyne meter",
        kg: "Kilogram",
		kgf: "Kilogram-force",
		kgfm: "Kilogram-force meter",
        g: "Gram",
		gf: "Gram-force",
		gfm: "Gram-force meter",
		oz: "Ounce",
		ozf: "Ounce-force",
        ozfin: "Ounce-force inch",
        ozfft: "Ounce-force foot",
		lb: "Pound",
		lbf: "Pound-force",
		lbfin: "Pound-force inch",
		lbfft: "Pound-force foot",
        rpm: "Revolutions per minute",
		Hz: "Hertz",
		rads: "Radian per second",
		W: "Watts",
        C: "Celcius",
        F: "Fahrenheit",
        K: "Kelvin",
        mps: "Meters per second",
        fts: "Feet per second",
        kph: "Kilometers per hour",
        mph: "Miles per hour",
        Pa: "Pascal",
        inH2O: "Inches of water",
        mmH2O: "Milimeters of water"
   }, 
   text:{  //how each unit will be printed in text
        N: "N",
        Nm: "N·m",
        dyn: "dyn",
        dynm: "dyn·m",
        kg: "kg",
        kgf: "kgf",
        kgfm: "kgf·m",
        g: "g",
        gf: "gf",
        gfm: "gf·m",
        oz: "oz",
        ozf: "ozf",
        ozfin: "ozf·in",
        ozfft: "ozf·ft",
        lb: "lb",
        lbf: "lbf",
        lbfin: "lbf·in",
        lbfft: "lbf·ft",
        rpm: "RPM",
        Hz: "Hz",
        rads: "rad/s",
        W: "W",
        C: "ºC",
        F: "ºF",
        K: "K",
        mps: "m/s",
        fts: "ft/s",
        kph: "km/h",
        mph: "mph",
        Pa: "Pa",
        inH2O: "in. H2O",
        mmH2O: "mm H2O"
    }, 
   choices:{  //those are in the dropdown lists for each sensor
        thrust: ["kgf","gf","ozf","lbf","N"], //unit chosen by user, but when GUI is loaded these will be defaults if nothing saved in storage
        torque: ["Nm","ozfin","ozfft","lbfin","lbfft","kgfm","gfm"],
        motorSpeed: ["rpm", "Hz", "rads"],
        weight: ["kg","g","oz","lb","N"],
		power: ["W"],
        temperature: ["C","F","K"],
        speed: ["mps", "fts", "kph", "mph"],
        pressure: ["Pa", "inH2O", "mmH2O"]
    },
    working:{  //units used in the backend of the GUI.
        thrust: "kgf", //units that the GUI is working with internally.
        torque: "Nm",
        motorSpeed: "rpm",
        weight: "kg",
        power: "W",
        temperature: "C",
        speed: "mps",
        pressure: "Pa"
        
    },
    display:{  //units shown to the user. Those are loaded/saved in chrome storage
        thrust: "kgf", //unit chosen by user, but when GUI is loaded these will be defaults if nothing saved in storage
        torque: "Nm",
        motorSpeed: "rpm",
        weight: "kg",
        power: "W",
        temperature: "C",
        speed: "mps",
        pressure: "Pa"
    },
    conversions:{ 
        //conversion coefficients for converting from working units -> display units. 
        //Use a function for other than scaling (ie temperature)
        //If using a function, conversions from other directions must be supplied (cannot do 1/coeff for reciprocal)
        kg:{ //base unit -> GUI uses these internally
            g: 1000, //display units (1000g in 1 kg)
            lb: 2.20462262,
            oz: 35.2739619,
			N: 9.80665002864,
			dyn: 980665.002864,
        },
		kgf:{ //base unit -> GUI uses these internally
            gf: 1000, //display units (1000gf in 1 kgf)
            lbf: 2.20462262,
            ozf: 35.2739619,
			N: 9.80665002864,
			dyn: 980665.002864,
        },
        Nm:{
            ozfin: 141.611928936,
            ozfft: 11.800994356504876,
            lbfin: 8.85074576738,
            lbfft: 0.737562149277,
			kgfm: 0.10197162129779283,
			gfm: 101.97162129779283,
			dynm: 100000,
        },
		rpm:{
			Hz: 0.016666666667,
			rads: 0.104719755120,
		},
        C:{
            F: function(C){
                return (C*9/5) + 32;
            },
            K: function(C){
                return C + 273.15;
            }
        },
        F:{
            C: function(F){
                return (F - 32) * 5/9;
            },
            K: function(F){
                return (F - 32) * 5/9 + 273.15;
            }
        },
        K:{
            C: function(K){
                return K - 273.15;
            },
            F: function(K){
                return (K - 273.15) * 9/5 + 32;
            }
        },
        mps:{
            fts: function(mps){
                return mps * 3.28084;
            },
            kph: function(mps){
                return mps * 3.6;
            },
            mph: function(mps){
                return mps * 2.2369363636364;
            }
        },
        Pa:{
            inH2O: function(Pa){
                return Pa / 248.84;
            },
            mmH2O: function(Pa){
                return Pa / 9.80665;
            }
        },
    },
    //converts the sensor value to user's unit system
    convertToDisplay: function (sensor, value){  //sensor would be the sensor in question, like "thrust"
		if(value === ''){
			return '';
		}else{
			var display = UNITS.display[sensor];
			var working = UNITS.working[sensor];
			if (working != display) {
				if(typeof value !== 'undefined'){
					var coefficient = UNITS.conversions[working][display];
                    var converted;
                    if(typeof(coefficient) === 'function'){
                        converted = coefficient(value);
                    }else{
                        converted = value * coefficient;
                    }
					return converted;
				}
			} else {
				return value * 1;
			}
		}
     },
	 //converts the sensor value to units used in the backend of the GUI
    convertToWorking: function (sensor, value){  //sensor would be the sensor in question, like "thrust"
		if(value === ''){
			return '';
		}else{
			var display =  UNITS.display[sensor];
			var working = UNITS.working[sensor];
			if (working != display) {
				if(typeof value !== 'undefined'){
					var coefficient = UNITS.conversions[working][display];
                    var converted = 0;
                    if(typeof(coefficient) === 'function'){
                        //Get the reciprocal function instead
                        coefficient = UNITS.conversions[display][working];
                        converted = coefficient(value);
                    }else{
                        converted = value / coefficient;
                    }
					return converted;
				}
			} else {
				return value * 1;
			}
		}
     },
     //returns the unit used by the user for a specific sensor
     getDisplayUnit: function (sensor){  //sensor would be the sensor in question, like "thrust"
          return UNITS.display[sensor];
     },
    //returns the text corresponding to an unit
    getUnitText: function (unit){  //unit is the unit id, like "kg"
          return UNITS.text[unit];
     },
    //returns the full text corresponding to an unit (to be displayed in the dropdown box)
    getUnitFullText: function (unit){  //unit is the unit id, like "kg"
          return UNITS.fullText[unit] + " [" + UNITS.getUnitText(unit) + "]";
     }
};
