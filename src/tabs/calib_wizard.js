function handleCalibration() {
	
	//returns the delay printed nicely (ie 5 minutes, 1 day, 3 months)
	function prettyDelay(delay){
		var pretty = '';
		var qty;
		if(delay>=30*3600*24*1000){
			qty = Math.floor(delay/(30*3600*24*1000));
			pretty += qty + ' month';
		}else if(delay>=3600*24*1000){
			qty = Math.floor(delay/(3600*24*1000));
			pretty += qty + ' day';
		}else if(delay>=3600*1000){
			qty = Math.floor(delay/(3600*1000));
			pretty += qty + ' hour';
		}else if(delay>=60*1000){
			qty = Math.floor(delay/(60*1000));
			pretty += qty + ' minute';
		}else{
			qty = Math.floor(delay/1000);
			pretty += qty + ' second';
		}
		if(qty>1)pretty+="s";
		return pretty;
	}

	//Torque calibration date
	chrome.storage.local.get('calibrateDynTorqueDate', function (result) {
        var connValid = CONFIGURATOR.connectionValid;
		var nowDate = new Date();
		var limit = 30*3600*24*1000; //30 days for the torque calibration
		var calibrateDynTorqueDate = result.calibrateDynTorqueDate;
		var never = calibrateDynTorqueDate === undefined;
		var delay = nowDate - calibrateDynTorqueDate;
		var old = delay > limit;
		var showWarning = never||old;
        
        //Left text warning
        if(CONFIG.boardVersion === "Series 1580"){
            $('#left-pane #info p.calibrationTorque').toggle(never&&connValid);
            $('#left-pane #info p.oldCalibrationTorque').toggle(old&&connValid);
        }else{
            $('#left-pane #info p.calibrationTorque').toggle(false);
            $('#left-pane #info p.oldCalibrationTorque').toggle(false);
        }
		
		//Delay under calibration button
		GUI.interval_remove('torqueDelayLabel');
		GUI.interval_add('torqueDelayLabel', function(){
			if(!connValid){
				$('label.calibrateDyn').text("");
			}else if(never){
				$('label.calibrateDyn').text(chrome.i18n.getMessage('initialSetupCalibrationNeverPerformed'));
			}else{
				var delay = new Date() - calibrateDynTorqueDate;
				$('label.calibrateDyn').text(chrome.i18n.getMessage('initialSetupCalibrationLastDate',[prettyDelay(delay)]));
			}	
		}, 1000, true); //update text every second
		
		//Thrust calibration date
		chrome.storage.local.get('calibrateDynThrustDate', function (result) {
			var limit = 30*3600*24*1000; //30 days for the thrust calibration
			var calibrateDynThrustDate = result.calibrateDynThrustDate;
			var never = calibrateDynThrustDate === undefined;
			var delay = nowDate - calibrateDynThrustDate;
			var old = delay > limit;
            
            if(LOAD_CELLS_CALIBRATION.calibrationFactorThrustCoeff){
                old = false; // a factory calibration never gets old
            }

			//Left text warning
            $('#left-pane #info p.calibrationThrust').toggle(never&&connValid);
            $('#left-pane #info p.oldCalibrationThrust').toggle(old&&connValid);
            if(CONFIG.boardVersion === "Series 1580"){
                $('#left-pane #info #warning').toggle((showWarning || never || old)&&connValid);
            }else{
                $('#left-pane #info #warning').toggle((never || old)&&connValid);
            }

			//Delay under calibration button
			GUI.interval_remove('thrustDelayLabel');
			GUI.interval_add('thrustDelayLabel', function(){
				if(!connValid){
					$('label.calibrateDyn').text("");
				}else if(never){
					$('label.calibrateDynThrust').text(chrome.i18n.getMessage('initialSetupCalibrationNeverPerformed'));
				}else{
					var delay = new Date() - calibrateDynThrustDate;
					$('label.calibrateDynThrust').text(chrome.i18n.getMessage('initialSetupCalibrationLastDate',[prettyDelay(delay)]));
				}	
			}, 1000, true); //update text every second
		});	
	});	

	chrome.storage.local.get('calibrationFactorLeft', function (result) {
		var calibrationFactorLeft = result.calibrationFactorLeft;	
		if(calibrationFactorLeft != undefined){
			LOAD_CELLS_CALIBRATION.calibrationFactorLeft = calibrationFactorLeft ;
			//console.log("Left calibration factor loaded: " + calibrationFactorLeft);
		}
	});

	chrome.storage.local.get('calibrationFactorRight', function (result) {
		var calibrationFactorRight = result.calibrationFactorRight;	
		if(calibrationFactorRight != undefined){
			LOAD_CELLS_CALIBRATION.calibrationFactorRight = calibrationFactorRight ;
			//console.log("Right calibration factor loaded: " + calibrationFactorRight);
		}
	});

	chrome.storage.local.get('calibrationFactorHingeLeft', function (result) {
		var calibrationFactorHingeLeft = result.calibrationFactorHingeLeft;	
		if(calibrationFactorHingeLeft != undefined){
			LOAD_CELLS_CALIBRATION.calibrationFactorHingeLeft = calibrationFactorHingeLeft;
			//console.log("Left hinge calibration factor loaded: " + calibrationFactorHingeLeft);
		}
	});

	chrome.storage.local.get('calibrationFactorHingeRight', function (result) {
		var calibrationFactorHingeRight = result.calibrationFactorHingeRight;	
		if(calibrationFactorHingeRight != undefined){
			LOAD_CELLS_CALIBRATION.calibrationFactorHingeRight = calibrationFactorHingeRight;
			//console.log("Right hinge calibration factor loaded: " + calibrationFactorHingeRight);
		}
	});

	chrome.storage.local.get('calibrationFactorThrust', function (result) {
		var calibrationFactorThrust = result.calibrationFactorThrust;	
		if(calibrationFactorThrust != undefined){
			LOAD_CELLS_CALIBRATION.calibrationFactorThrust = calibrationFactorThrust ;
			//console.log("Thrust calibration factor loaded: " + calibrationFactorThrust);
		}
	});
    
    chrome.storage.local.get('calibrationFactorThrustCoeff', function (result) {
		var calibrationFactorThrustCoeff = result.calibrationFactorThrustCoeff;	
		if(calibrationFactorThrustCoeff != undefined){
			LOAD_CELLS_CALIBRATION.calibrationFactorThrustCoeff = calibrationFactorThrustCoeff ;
			//console.log("Thrust calibration factor factory flag: " + calibrationFactorThrustCoeff);
		}
	});

	var isConnected = GUI.connected_to && (GUI.connected_to.valueOf().trim() != "");
	if (isConnected) {
		$('#content .tab-setup a.calibrateDyn').css('opacity', '1.0');
		$('#content .tab-setup a.calibrateDynThrust').css('opacity', '1.0');
		$('#content .tab-setup a.calibrateDyn').css('pointer-events', '');
		$('#content .tab-setup a.calibrateDynThrust').css('pointer-events', '');
	}else{
		$('#content .tab-setup a.calibrateDyn').css('opacity', '0.5');
		$('#content .tab-setup a.calibrateDyn').css('pointer-events', 'none');
		$('#content .tab-setup a.calibrateDynThrust').css('opacity', '0.5');
		$('#content .tab-setup a.calibrateDynThrust').css('pointer-events', 'none');
	}
}

function calib_wizard(type) {
	calib_wizard.cal_step = 1;
	var testWeightKg = LOAD_CELLS_CALIBRATION.calibrationWeight; //Kg
	var testWeightGrams = (1000*testWeightKg).toFixed(0); //grams, for display
	var tareTime = TARE_OFFSET.tareTime; //ms
	var hingeDistance = LOAD_CELLS_CALIBRATION.hingeDistance; //meters
	var thrustLoadCellSensitivity = LOAD_CELLS_CALIBRATION.thrustNominal/LOAD_CELLS_CALIBRATION.fullScaleVoltage; // kg/mV
	var torqueLoadCellSensitivity = LOAD_CELLS_CALIBRATION.leftRightNominal/LOAD_CELLS_CALIBRATION.fullScaleVoltage; // kg/mV

	// Object used to hold temporary information
	function _temp_value() {
		this.loadCellResetValue = 0; // load cell tare value
		this.raw = [0, 0]; // raw load cell readings buffered here
		this.offset = function (id) {return this.raw[id] - this.loadCellResetValue;}; //returns offset compensated mV reading
	}

	function calibrationSuccess(message){
		$("#calibration-instruction").html(message);
		$("a.calibrateDynAbort.buttons").html("Close window");
		$("a.calibrateDynNext.buttons").html("Redo calibration");
		$('a.calibrateDynNext.buttons').unbind("click");
		$('a.calibrateDynNext.buttons').click(function(){loadWizard(type);});
		$("#calib-img").hide();
		handleCalibration();
	}

	function calibrationFailed(reason){
		$("#calibration-instruction").html("<span style=\"color: red\">" + reason + "</span>");
		$("a.calibrateDynAbort.buttons").html("Close window");
		$("a.calibrateDynNext.buttons").html("Retry");
		$('a.calibrateDynNext.buttons').unbind("click");
		$('a.calibrateDynNext.buttons').click(function(){loadWizard(type);});
		$("#calib-img").hide();
        $("#calib-input-box").hide();
		googleAnalytics.sendException(reason, false);
	}

	function showWait(){
		$("a.calibrateDynNext.buttons").html("Please wait...");
	}

	function loadWizard(type){        
        // Load html and call function to process it
		if (type === "torque")
			process_wizard_torque();
		else if  (type === "thrust")
			process_wizard_thrust();
	}

	// Calibration canceled by user
	function abortCalib () {
		$('button.ui-dialog-titlebar-close').click();
	};

	$('a.calibrateDynAbort.buttons').unbind("click");
	$('a.calibrateDynAbort.buttons').click(abortCalib);

	// Function executed when the wizard is launched. 
	function process_wizard_torque() {
		// Initializing step count. The calibration has 3 steps
		calib_wizard.cal_step = 1;
        $("#calib-input-box").hide();

		// The following values will only be saved only at the end of the calibration
		// They will not be saved if the user aborts.
		// First, we need a container for the saved values
		var calWiz = { 
		  left  : new _temp_value(),
		  right  : new _temp_value(),
		};

		$("#calib-img").show();
		$("#calib-img").attr("data","/images/calibration_torque_step_1.svg");
		$("#calibration-instruction").html("Please install the calibration bar as shown on the image and press next.");
		$("a.calibrateDynNext.buttons").html("Next step (1/3)");
		$("a.calibrateDynAbort.buttons").html("Cancel");

		// Click of next button
		$('a.calibrateDynNext.buttons').unbind("click");
		$('a.calibrateDynNext.buttons').click(function () {

			//Go to next step 
			calib_wizard.cal_step = calib_wizard.cal_step+1;
			$("#cal-step").html(calib_wizard.cal_step);

			// The switch handles calibration and diplay of instructions
			switch(calib_wizard.cal_step) {
			  case 2:
				// Wait for lpf to stabilize
				LPFLeft.forceNextValue();
    			LPFRight.forceNextValue();
				showWait();
				setTimeout(function(){ // Display next page when calib is over
				  calWiz.left.loadCellResetValue = DATA.getLoadCellLeft(); //raw load cell mV
				  calWiz.right.loadCellResetValue = DATA.getLoadCellRight(); //raw load cell mV  
				  $("#calib-img").attr("data","/images/calibration_torque_step_2.svg");
				  $("#calibration-instruction").html("Please place the " + testWeightGrams + "g weight against the center screws as shown on the image");
				  $("a.calibrateDynNext.buttons").html("Next step (2/3)");
				}, tareTime);
				break;
			  case 3:
				// Wait for lpf to stabilize
				LPFLeft.forceNextValue();
    			LPFRight.forceNextValue();
				showWait();
				setTimeout(function(){ // Display next page when calib is over
				  calWiz.left.raw[0] = DATA.getLoadCellLeft(); //raw load cell mV
				  calWiz.right.raw[0] = DATA.getLoadCellRight(); //raw load cell mV 
				  $("#calib-img").attr("data","/images/calibration_torque_step_3.svg");
				  $("#calibration-instruction").html("Please place the " + testWeightGrams + "g weight against the extremity screws as shown on the image");
				  $("a.calibrateDynNext.buttons").html("Next step (3/3)");
				}, tareTime);
				break;
			  case 4:
				// Wait for lpf to stabilize
				LPFLeft.forceNextValue();
    			LPFRight.forceNextValue();
				showWait();
				setTimeout(function(){ // Display next page when calib is over
					calWiz.left.raw[1] = DATA.getLoadCellLeft(); //raw load cell mV
				    calWiz.right.raw[1] = DATA.getLoadCellRight(); //raw load cell mV 

				    // 1 - calculate load cell calibration coefficients
					
					//Theoretical force in the two positions (Newton)
					var g = CONFIG.gravityConstant;
					var N = testWeightKg*g;
					var F = math.matrix([ [N], 
										  [N] ]);
					
					//Phi matrix -> Measured force (Newtons) in the two positions
					var toN = torqueLoadCellSensitivity*g; //convert mV to Newtons
					var phi = math.matrix(
                  		[[toN*calWiz.left.offset(0), toN*calWiz.right.offset(0)], 
                   		 [toN*calWiz.left.offset(1), toN*calWiz.right.offset(1)] ]);
					
                   	//C is calibration matrix linking real measurements and theoretical values found as:
                    var C;
                    try{
                        var C = math.multiply(math.inv(phi), F);
                    }catch(e){
                        calibrationFailed("Could not calculate hinge separation distance. Make sure that both torque load cells are responding and try again. To check load cells, please active debug mode (from the setup tab). Resetting the board (disconnect and reconnect USB) may help.");    
                    }
					
					//Extract the calibration coefficents
					var CL = C.valueOf()[0][0];
                	var CR = C.valueOf()[1][0];
					
					if((Math.abs(CL) <= 1.25 && Math.abs(CL) >= 0.75 && Math.abs(CR) <= 1.25 && Math.abs(CR) >= 0.75) || SYSTEM_LIMITS.calibrationAlwaysPass){
						// 2 - calibrate load cell separation coefficient

						//Get the measured load matrix
						var theta = math.matrix([
						  [-calWiz.left.offset(0)*CL*toN, calWiz.right.offset(0)*CR*toN], 
						  [-calWiz.left.offset(1)*CL*toN, calWiz.right.offset(1)*CR*toN]
						  ]);

						//Generate the ideal torque matrix (from center of hinges)
						var TauIdeal = N*LOAD_CELLS_CALIBRATION.calibrationWeightDistance; //Nm
						var M = math.matrix([ [0], [TauIdeal] ]);

						//Calculate the hinge distances from center on both sides
						var D = math.multiply(math.inv(theta), M);

						//Calculate the hinge separation
						var HL = D.valueOf()[0][0];
						var HR = D.valueOf()[1][0];

						//Get the hinge separation calibration coefficient
						var CDL = 2*HL/LOAD_CELLS_CALIBRATION.hingeDistance;
						var CDR = 2*HR/LOAD_CELLS_CALIBRATION.hingeDistance;
						var CD = 0.5*(CDL+CDR);
                        var accRange = 0.075;
                        
						if((CD <= 1+accRange && CD >= 1-accRange) || SYSTEM_LIMITS.calibrationAlwaysPass){
							//save the new calibration
							LOAD_CELLS_CALIBRATION.calibrationFactorLeft = CL;
							LOAD_CELLS_CALIBRATION.calibrationFactorRight = CR;
							LOAD_CELLS_CALIBRATION.calibrationFactorHingeLeft = CDL;
							LOAD_CELLS_CALIBRATION.calibrationFactorHingeRight = CDR;
							chrome.storage.local.set({'calibrationFactorLeft': CL});
							chrome.storage.local.set({'calibrationFactorRight': CR});
							chrome.storage.local.set({'calibrationFactorHingeLeft': CDL});
							chrome.storage.local.set({'calibrationFactorHingeRight': CDR});

							//save calibrateDynTorque date
							var date = +new Date;
							chrome.storage.local.set({'calibrateDynTorqueDate': date});

							calibrationSuccess("<span style=\"color: green\">Torque and weight calibration sucessfull! Do not forget to tare torque and weight before using device.</span><br><br>It is recommended to perform a new torque calibration everytime something has changed (motor, wiring, fastener tightening, etc...)<br><br>Tip: to verify, manually perform a torque and weight tare. Then while the bar is still installed, you may check that the test weight reads " + testWeightGrams + "g anywhere on the bar. You can then confirm the torque is zero at the first position, and " + TauIdeal.toFixed(3) +  "Nm at the second position.");
							console.log("Torque calibration coefficients:");
							console.log("    -CL:" + CL);
							console.log("    -CR:" + CR);
							console.log("    -CDL:" + CDL);
							console.log("    -CDR:" + CDR);
						}else{
				            calibrationFailed("Hinge calibration coefficients (" + CDL.toFixed(4) + " and " + CDR.toFixed(4) + ") out of acceptable range. Make sure the hardware is properly attached, all fasteners are tight and the two torque load cells are parallel to each other. Also make sure you have not reversed the left and right load cells and verify you have not swapped the thrust load cell with one of the torque load cells (the thrust load cell should be in the middle connector).");
                        }
						
					} else {
						calibrationFailed("Torque calibration coefficients (" + CR.toFixed(4) + " and " + CL.toFixed(4) + ") out of acceptable range. Your calibration coefficients seem to indicate that you inverted two load cells, or two load cell connections. Verify you have not swapped the thrust load cell with one of the torque load cells (the thrust load cell should be in the middle connector).");
                    }
				}, tareTime);
				break;
			}
		});  
	}
    
	function process_wizard_thrust() {
        console.log("Preparing thrust wizard");
        
        // user supplied calibration weight limits
        var minWeight = 50;
        var maxWeight = 5000;
        $("#calib-input-box").hide();
        $("#calib-img").hide();
        
        if(CONFIG.boardVersion === "Series 1520"){
            // ask user what type of calibration he/she wants to do
            $("#calibration-instruction").html("There are two ways to calibrate your Series 1520 unit:<br/><br/><b>Option A</b>:<br/>Use our factory calibration sticker attached to the load cell.<br/><br/><b>Option B</b>:<br/>If you have your own calibration weight, between " + minWeight + "g and " + maxWeight + "g.<br/><br/>");
            
            $('a.calibrateDynNext.buttons').html("Option B: weight");
            $('a.calibrateDynNext.buttons').unbind("click"); 
            $('a.calibrateDynNext.buttons').click(function(){
                // asks user to type in weight
                $("#calib-input-box").show();
                $("#calib-input-text").html("Calibration weight (g)");
                $("#calibration-instruction").html("Please enter the mass of your calibration weight between " + minWeight + " and " + maxWeight + " grams.");
                
                // prepare next step
                $("a.calibrateDynAbort.buttons").html("Cancel");
                $('a.calibrateDynAbort.buttons').unbind("click"); 
                $('a.calibrateDynAbort.buttons').click(abortCalib);
                $("a.calibrateDynNext.buttons").html("Next");
                $('a.calibrateDynNext.buttons').unbind("click");
                $('a.calibrateDynNext.buttons').click(function(){
                    // get entered value
                    var weight = Number($("#calib-input-input").val());
                    if(weight >= minWeight && weight<=maxWeight){
                        $("#calib-input-box").hide();
                        calibrateThrustWithWeight(weight);
                    }else{
                        calibrationFailed("The calibration weight (" + weight + "g) is not within the acceptable range (" + minWeight + "-" + maxWeight + "g)");
                    }
                });
            });
            
            $('a.calibrateDynAbort.buttons').html("Option A: factory");
            $('a.calibrateDynAbort.buttons').unbind("click");
            $('a.calibrateDynAbort.buttons').click(function(){
                // asks user for calibration coefficient
                $("#calib-input-box").show();
                $("#calib-input-text").html("Calibration coefficient (format 000-000-00)");
                $("#calibration-instruction").html("Please locate the calibration sticker attached to the load cell. You must enter the complete number including dashes and zeros.");
                
                // prepare next step
                $("a.calibrateDynAbort.buttons").html("Cancel");
                $('a.calibrateDynAbort.buttons').unbind("click"); 
                $('a.calibrateDynAbort.buttons').click(abortCalib);
                $("a.calibrateDynNext.buttons").html("Apply");
                $('a.calibrateDynNext.buttons').unbind("click");
                $('a.calibrateDynNext.buttons').click(function(){
                    // get entered value
                    var coeffString = $("#calib-input-input").val();
                    if(coeffString.match(/[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]/) || coeffString === "000-000-00"){
                        
                        //Coef is made of first 6 numbers, and remaining 2 are a checksum
                        
                        $("#calib-input-box").hide();
                        
                        var checksum = Number(coeffString.charAt(8) + coeffString.charAt(9));
                        var sum = 0;
                        sum += Number(coeffString.charAt(0));
                        sum += Number(coeffString.charAt(1));
                        sum += Number(coeffString.charAt(2));
                        sum += Number(coeffString.charAt(4));
                        sum += Number(coeffString.charAt(5));
                        sum += Number(coeffString.charAt(6));
                        
                        var coeff;
                        if(Number(coeffString.charAt(0)) >= 5){
                            //coef is something like 0.98123
                            coeff = "0.";
                        }else{
                            //coef is something like 1.0124
                            coeff = "1.";
                        }
                        coeff += coeffString.charAt(0);
                        coeff += coeffString.charAt(1);
                        coeff += coeffString.charAt(2);
                        coeff += coeffString.charAt(4);
                        coeff += coeffString.charAt(5);
                        coeff += coeffString.charAt(6);
                        coeff = Number(coeff);
                        
                        // convert into a calibration coefficient
                        if(!isNaN(coeff) && coeff>=0.75 && coeff<=1.25 && sum===checksum){
                            //save the new calibration
                            LOAD_CELLS_CALIBRATION.calibrationFactorThrust = coeff;
                            chrome.storage.local.set({'calibrationFactorThrust': coeff});
                            
                            LOAD_CELLS_CALIBRATION.calibrationFactorThrustCoeff = true;
                            chrome.storage.local.set({'calibrationFactorThrustCoeff': true});

                            calibrationSuccess("<span style=\"color: green\">Thrust calibration coefficient sucessfully applied");
                            console.log("Thrust calibration coefficient: " + coeff);

                            //save calibrateDynThrust date
                            var date = +new Date;
                            chrome.storage.local.set({'calibrateDynThrustDate': date});
                        }else{
                            calibrationFailed("The entered calibration coefficient is invalid, please try again.");
                        }
                    }else{
                        calibrationFailed("The calibration coefficient entered does not match the required pattern xxx-xxx-xx.");
                    }
                });
            });
            
            
        }else if(CONFIG.boardVersion === "Series 1580"){
            calibrateThrustWithWeight(testWeightGrams)
        }else{
            console.error("Invalid board: " + CONFIG.boardVersion);
        }
        
		function calibrateThrustWithWeight(grams){
            
            // Initializing step count. The calibration has 3 steps
            calib_wizard.cal_step = 1;

            // The following values will only be saved only at the end of the calibration
            // They will not be saved if the user aborts.
            // First, we need a container for the saved values
            var calWiz = { 
              thrust  : new _temp_value(),
            };

            $("#calib-img").show();
            if(CONFIG.boardVersion === "Series 1520"){
                $("#calib-img").attr("data","/images/calibration_basic_1.svg");
            }else{
                $("#calib-img").attr("data","/images/calibration_thrust_step_1.svg");
            }
            
            $("#calibration-instruction").html("Please install the device vertically as shown on the image using the leg of a table or equivalent method.");
            $("a.calibrateDynNext.buttons").html("Next step (1/2)");
            $("a.calibrateDynAbort.buttons").html("Cancel");

            // Click of next button
            $('a.calibrateDynNext.buttons').unbind("click");
            $('a.calibrateDynNext.buttons').click(function () {		

                //Go to next step 
                calib_wizard.cal_step = calib_wizard.cal_step+1;
                $("#cal-step").html(calib_wizard.cal_step);

                // The switch handles calibration and diplay of instructions
                switch(calib_wizard.cal_step) {
                  case 2:
                    // Wait for lpf to stabilize
                    LPFThrust.forceNextValue();
                    showWait();
                    setTimeout(function(){ // Display next page when calib is over
                      calWiz.thrust.loadCellResetValue = DATA.getLoadCellThrust(); //raw load cell mV
                      if(CONFIG.boardVersion === "Series 1520"){
                        $("#calib-img").attr("data","/images/calibration_basic_2.svg");
                          
                        function replaceText(loop){
                            // avoid infinite loop in case SVG image was modified
                            if(loop>0){
                                // replace the weight's text inside the SVG image (svg loading takes some time)
                                setTimeout(function(){
                                    var weightText = $('#showCalibWizard #calib-img')[0].contentDocument.getElementById('tspan4138');
                                    if(weightText){
                                        weightText.textContent = grams + "g"; 
                                        console.log("Replacing text");
                                    }else{
                                        console.log("Waiting");
                                        replaceText(loop-1);
                                    }
                                }, 10);   
                            }else{
                                console.error("Could not find the test weight's svg attribute.");
                            }
                        }
                          
                        replaceText(100);                      
                      }else{
                        $("#calib-img").attr("data","/images/calibration_thrust_step_2.svg");
                      }
                      $("#calibration-instruction").html("Please place the " + grams + "g weight on the motor mounting plate as shown on the image.");
                      $("a.calibrateDynNext.buttons").html("Next step (2/2)");
                    }, tareTime);
                    break;
                  case 3:
                    // Wait for lpf to stabilize
                    LPFThrust.forceNextValue();
                    showWait();
                    setTimeout(function(){ // Display next page when calib is over
                        // Take data
                        calWiz.thrust.raw[0] = DATA.getLoadCellThrust(); //raw load cell mV

                        //Calculate calibration coefficient
                        var calib = 0.001/((calWiz.thrust.offset(0)*thrustLoadCellSensitivity)/grams); // [mV*kg/mV]/kg

                        if((Math.abs(calib) <= 1.25 && Math.abs(calib) >= 0.75) || SYSTEM_LIMITS.calibrationAlwaysPass){
                            //save the new calibration
                            LOAD_CELLS_CALIBRATION.calibrationFactorThrust = calib;
                            chrome.storage.local.set({'calibrationFactorThrust': calib});
                            
                            //not using factory calibration
                            LOAD_CELLS_CALIBRATION.calibrationFactorThrustCoeff = false;
                            chrome.storage.local.set({'calibrationFactorThrustCoeff': false});

                            calibrationSuccess("<span style=\"color: green\">Thrust calibration sucessfull! Do not forget to tare thrust after reinstalling device.</span><br><br>Tip: as a verification, while the device is still vertical, you may check that the thrust is " + grams + "g after removing the weight, taring, and putting the weight back.");
                            console.log("Thrust calibration coefficient: " + calib);

                            //save calibrateDynThrust date
                            var date = +new Date;
                            chrome.storage.local.set({'calibrateDynThrustDate': date});
                        }else{
                            var additionalTroubleshooting = "";
                            if(CONFIG.boardVersion === "Series 1580"){
                                additionalTroubleshooting = "Also verify you have not swapped the thrust load cell with one of the torque load cells (the thrust load cell should be in the middle connector)."
                            }
                            calibrationFailed("Thrust calibration coefficient (" + calib.toFixed(4) + ") out of acceptable range. Make sure the load cell is properly connected. " + additionalTroubleshooting);
                        }
                    }, tareTime);
                    break;
                }
            });
        }
	}

	loadWizard(type);

	localize();
}