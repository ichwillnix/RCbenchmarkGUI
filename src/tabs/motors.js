'use strict';

TABS.motors = {};
TABS.motors.initialize = function (callback) {
    var self = this;
    var minCommand = [CONFIG.ESCMin.value, CONFIG.servo1Min.value, CONFIG.servo2Min.value, CONFIG.servo3Min.value];
    var maxCommand = [CONFIG.ESCMax.value, CONFIG.servo1Max.value, CONFIG.servo2Max.value, CONFIG.servo3Max.value];

    if (GUI.active_tab != 'motors') {
        GUI.active_tab = 'motors';
        googleAnalytics.sendAppView('Motors');
    }

    $('#tabContent').load("./tabs/motors.html", process_html);
    
    function process_html() {
        // translate to user-selected language
        localize();
		if(logSample.continuousLog)
			$('#continousLog').prop('checked', true);
		logCallbacksSetup();
		
        var number_of_valid_outputs = 4;

        $('#content div.sliders input').each(function (index) {
            $(this).prop('min', minCommand[index]);
            $(this).prop('max', maxCommand[index]);
            $(this).val(minCommand[index]);
        });
        $('#content div.values li:not(:last)').each(function (index) {
            $(this).text(minCommand[index]);
		});
        // callback when a slider is moved
        $('#content div.sliders input:not(.master)').on('input', function () {
            var index = $(this).index();
            var val = $(this).val();

            // set the text value under the sliders
            $('#content div.values li').eq(index).text(val);

            // save
            if(index==0) OUTPUT_DATA.ESC_PWM = Number(val);
            if(index>0 && index<number_of_valid_outputs) OUTPUT_DATA.Servo_PWM[index-1] = Number(val);
        });

        // hide sliders not used
        $('#content ul.titles li').slice(number_of_valid_outputs).hide();
        $('#content div.sliders input').slice(number_of_valid_outputs).hide();
        $('#content div.checkboxes input').slice(number_of_valid_outputs).hide();
        $('#content div.values li').slice(number_of_valid_outputs).hide();

        // handle when a slider is actived
        $('#content div.checkboxes input[type="checkbox"]').change(function () {
            var index = $(this).index();
			
			// disactivate safety cutoff (if active)
            if(index==0) {
            	safetyCutoffActivated = false;
				userCutoff = false;
            }

            if ($(this).is(':checked')) {
				// enable output
				OUTPUT_DATA.active[index] = 1;
				$('#content .tab-motors p.cutoff').hide();
            } else {
                // disable output
                OUTPUT_DATA.active[index] = 0;

                // change value to default (for motor only)
                if(index==0) $('#content div.sliders input').eq(index).val(CONFIG.ESCCutoff.value);

                // trigger change event so value are sent to mcu
                if(index==0) $('#content div.sliders input').eq(index).trigger('input');
            }
        });

        //adjust sliders to current values
        var sliders = $('#content div.sliders input');
        for(var i=0; i<number_of_valid_outputs; i++){
            if(i==0) sliders.eq(i).val(OUTPUT_DATA.ESC_PWM);
            if(i>0) sliders.eq(i).val(OUTPUT_DATA.Servo_PWM[i-1]);
            $('#content div.checkboxes input').eq(i).attr('checked', OUTPUT_DATA.active[i]!=0);
        }

        // only fire events when all values are set
        sliders.trigger('input');

        // enable all sliders
        sliders.prop('disabled', false);

        logSample.updateLogInfo();

        if (callback) callback();
    }
};

TABS.motors.cleanup = function (callback) {
    if (callback) callback();
};