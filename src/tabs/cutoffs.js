'use strict';

var SCALES = { //define scales
    voltage: 0.5,			
    current: 0.5,
    currentBurst: 0.5,
    power: 10,
    thrust: 0.1,			
    torque: 0.1,		
    motorSpeed: 10,
    vibration: 0.1,
    temperature: 1
};

//Variables for the safety cutoff
var safetyCutoffActivated = false;
var lastCutoff = '';
var userCutoff = false;
var wasSafetyCutoffActivated = false; //used to determine when it is appropriate to notify the logging (ie only once)
function safetyLimits(){
    var logFileCutoffTxt = "";
    
	//Check safety limits
	var exceeding = [];
	var exceeded = []; 
	if(!CONFIG.safetyCutoffDisable){
        
        // makes the text glow orange if its limit is exceeded
        function highlight(state, elemName) {
            if(state)
                $('#data-pane #data dt.' + elemName).css('color', '#ff9201');
            else
                $('#data-pane #data dt.' + elemName).css('color', 'white');
        }
        
        // checks the limits
        // valFct can also be a direct value (not a function)
        // custom
        function checkLimit(id, valFct, min, max, customLabel){
            var val = valFct;
            if (typeof valFct === "function") {
                val = valFct();
            }
            if(val < min || val > max){
                if(customLabel){
                    exceeded.push(customLabel);
                    if(exceeding.indexOf(customLabel) === -1){
                        exceeding.push(customLabel);
                    }
                }else{
                    exceeded.push(id);
                    if(exceeding.indexOf(id) === -1){
                        exceeding.push(id);
                    }
                }
                
                highlight(true, id);
                
                var exceedVal;
                if(val < min){
                    exceedVal = min;
                }else{
                    exceedVal = max;
                }
                logFileCutoffTxt = "Cutoff: " + id + " (" + exceedVal + ")";
            }else{
                highlight(false, id);
            }
        }
        
        checkLimit('voltage', DATA.getESCVoltage, USER_LIMITS.voltageMin, USER_LIMITS.voltageMax);
        checkLimit('currentBurst', DATA.getESCCurrent, USER_LIMITS.currentBurstMin, USER_LIMITS.currentBurstMax);
        checkLimit('current', LPFescCurrentBurst.getValue, USER_LIMITS.currentMin, USER_LIMITS.currentMax);
        checkLimit('power', DATA.getElectricalPower, USER_LIMITS.powerMin, USER_LIMITS.powerMax);
        checkLimit('thrust', DATA.getThrust, USER_LIMITS.thrustMin, USER_LIMITS.thrustMax);
        checkLimit('torque', DATA.getTorque, USER_LIMITS.torqueMin, USER_LIMITS.torqueMax);
        
        switch(CONFIG.mainRPMsensor.value) {
            case "electrical":
                checkLimit('rpmElectrical', DATA.getRPM, USER_LIMITS.rpmMin, USER_LIMITS.rpmMax);
                break;
            case "optical":   
                checkLimit('rpmOptical', DATA.getRPM, USER_LIMITS.rpmMin, USER_LIMITS.rpmMax);
                break;
            default:
                console.error("Unknown rpm sensor type");
        }

		checkLimit('vibration', SENSOR_DATA.vibration, USER_LIMITS.vibrationMin, USER_LIMITS.vibrationMax);
        
        var temperatures = DATA.getTemperatures();
        for(var i=0; i<temperatures.length; i++){
            var temp = temperatures[i];
            var id = temp.id;
            var label = temp.label;
            var textId = "temp" + id;
            
            var userMin = USER_LIMITS.temperaturesMin[id];
            var userMax = USER_LIMITS.temperaturesMax[id];
            var exceedLabel = 'temperature (' + label + ')';
            checkLimit(textId, temp.value, userMin, userMax, exceedLabel);
        }; 
        
		if(exceeded.length != 0 || userCutoff) safetyCutoffActivated = true;
	}else{
		safetyCutoffActivated = false;
	}

	//Generate the text explaining what limits are exceeded
	var exceededText = '';
	var limitsExceededText = '';
	if(exceeding.length > 0){
		exceededText = '*** ';
		if(exceeding.length != 0){
			for (var i = 0; i < exceeding.length; i++){
				if(i == exceeding.length - 1)
					limitsExceededText += exceeding[i].capitalizeFirstLetter();
				else if(i == exceeding.length - 2)
					limitsExceededText += exceeding[i].capitalizeFirstLetter() + ' and ';
				else
					limitsExceededText += exceeding[i].capitalizeFirstLetter() + ', ';
			}
			exceededText += limitsExceededText;
		}
		exceededText +=  ' safety limit';
		if(exceeding.length > 1) exceededText += 's';
		exceededText += ' exceeded! ***'
		lastCutoff = "(" + limitsExceededText.toLowerCase() + ") ";
	}

	//Handle the cutoff
	var autoReset = exceeded.length===0 && (!OUTPUT_DATA.active[0] && OUTPUT_DATA.ESC_PWM === 1000);
	if(autoReset) {
		safetyCutoffActivated = false;
		userCutoff = false;
	}
	if(safetyCutoffActivated){
		$('#content div.sliders input:first').prop('disabled', true);
		$('#content div.sliders input:first').css('opacity', '0.5');
		$('#content div.sliders input:first').val(CONFIG.ESCCutoff.value);
		$('#content div.values li:first').text(CONFIG.ESCCutoff.value);
		$('#content .tab-motors p.cutoff').show();
		if(exceeding.length > 0){
			$('#content .tab-motors p.cutoff').html(exceededText);	
			if(OUTPUT_DATA.active[0]) scriptReportError(exceededText);
		}else{
			if(userCutoff){
				$('#content .tab-motors p.cutoff').html("*** Spacebar was pressed to cut motors.<br>Recheck ESC slider to reset. ***");
				$('#data-pane #data dt.cutoff').html("SPACEBAR was pressed.");
				if(OUTPUT_DATA.active[0]) scriptReportError("*** Spacebar was pressed ***");
			}else{
				$('#content .tab-motors p.cutoff').html("*** Cutoff " + lastCutoff + "was activated.<br>Recheck ESC slider to reset. ***");	
				$('#data-pane #data dt.cutoff').html(exceededText);
			}
		}
		$('#data-pane #data dt.cutoff').show();
		OUTPUT_DATA.ESC_PWM = CONFIG.ESCCutoff.value;
	} else {
		$('#content div.sliders input:first').prop('disabled', false);
		$('#content div.sliders input:first').css('opacity', '1');
		$('#data-pane #data dt.cutoff').hide();
		$('#content .tab-motors p.cutoff').hide();
	}
    
    //handle notification to the log file
    if(safetyCutoffActivated){
        if(!wasSafetyCutoffActivated){
            if(userCutoff){
                logSample.logAppMessage("Cutoff: spacebar was pressed");
            }else{
                logSample.logAppMessage(logFileCutoffTxt);
            }
        }
    }
    
    wasSafetyCutoffActivated = safetyCutoffActivated;
}

function refreshCutoffs(){
	var temperatures = DATA.getTemperatures();
    
    //RPM sensor system limit
    var max_eRPM = SYSTEM_LIMITS.erpmMax || 500000; //in case of hack code used to increase this limit.
    switch(CONFIG.mainRPMsensor.value) {
        case 'electrical':
            SYSTEM_LIMITS.rpmMax = Math.floor(2*max_eRPM/CONFIG.numberOfMotorPoles.value / 1000)*1000;
            break;
        case 'optical':
            SYSTEM_LIMITS.rpmMax = Math.floor(2*max_eRPM/CONFIG.numberOfOpticalTape.value / 1000)*1000;
            break;
        default:
            throw "Sensor not recognized"
    }
    
    //Clip user limits if above system limits (needed because of hack codes)
    if(USER_LIMITS.voltageMax > SYSTEM_LIMITS.voltageMax) USER_LIMITS.voltageMax = SYSTEM_LIMITS.voltageMax;
    if(USER_LIMITS.currentMax > SYSTEM_LIMITS.currentMax) USER_LIMITS.currentMax = SYSTEM_LIMITS.currentMax;
    if(USER_LIMITS.currentBurstMax > SYSTEM_LIMITS.currentBurstMax) USER_LIMITS.currentBurstMax = SYSTEM_LIMITS.currentBurstMax;
    if(USER_LIMITS.powerMax > SYSTEM_LIMITS.powerMax) USER_LIMITS.powerMax = SYSTEM_LIMITS.powerMax;
    if(USER_LIMITS.rpmMax > SYSTEM_LIMITS.rpmMax) USER_LIMITS.rpmMax = SYSTEM_LIMITS.rpmMax;
    if(USER_LIMITS.thrustMax > SYSTEM_LIMITS.thrustMax) USER_LIMITS.thrustMax = SYSTEM_LIMITS.thrustMax;
    if(USER_LIMITS.thrustMin < SYSTEM_LIMITS.thrustMin) USER_LIMITS.thrustMin = SYSTEM_LIMITS.thrustMin;
    if(USER_LIMITS.torqueMin < SYSTEM_LIMITS.torqueMin) USER_LIMITS.torqueMin = SYSTEM_LIMITS.torqueMin;
    if(USER_LIMITS.torqueMax > SYSTEM_LIMITS.torqueMax) USER_LIMITS.torqueMax = SYSTEM_LIMITS.torqueMax;
    if(USER_LIMITS.vibrationMax > SYSTEM_LIMITS.vibrationMax) USER_LIMITS.vibrationMax = SYSTEM_LIMITS.vibrationMax;
    for(var i=0; i<temperatures.length; i++){
        var temp = temperatures[i];

        var id = temp.id;
        var sysMax = SYSTEM_LIMITS.temperatureMax;
        var sysMin = SYSTEM_LIMITS.temperatureMin;
        var userMin = USER_LIMITS.temperaturesMin[id];
        var userMax = USER_LIMITS.temperaturesMax[id];
        
        // assign default if never set
        if(userMin === undefined || userMax === undefined){
            userMin = USER_LIMITS.temperatureMin;
            userMax = USER_LIMITS.temperatureMax;
        }
        
        // check range
        if(userMin < sysMin) userMin = sysMin;
        if(userMax > sysMax) userMax = sysMax;
        
        // write back user limits
        USER_LIMITS.temperaturesMin[id] = userMin;
        USER_LIMITS.temperaturesMax[id] = userMax;
    };  
	
    //GUI cutoffs management
	//display user units
	$('.tab-cutoffs #thrustUnit').html('Thrust (' + UNITS.text[UNITS.display['thrust']] + ')');
	$('.tab-cutoffs #torqueUnit').html('Torque (' + UNITS.text[UNITS.display['torque']] + ')');
	$('.tab-cutoffs #motorSpeedUnit').html('Motor Rotation Speed (' + UNITS.text[UNITS.display['motorSpeed']] + ')');
    for(var i=0; i<temperatures.length; i++){   
        var temp = temperatures[i];
        var id = temp.id;
        var label = temp.label;
        $('.tab-cutoffs #temperatureUnit' + (i+1)).html(label + ' Temperature (' + UNITS.text[UNITS.display['temperature']] + ')');
    }
    
	//variables
	var voltageMinGUI = $('.tab-cutoffs #voltageMin');
	var voltageMaxGUI = $('.tab-cutoffs #voltageMax');
	var currentMinGUI = $('.tab-cutoffs #currentMin');
	var currentMaxGUI = $('.tab-cutoffs #currentMax');
    var currentBurstMinGUI = $('.tab-cutoffs #currentBurstMin');
	var currentBurstMaxGUI = $('.tab-cutoffs #currentBurstMax');
    var powerMinGUI = $('.tab-cutoffs #powerMin');
	var powerMaxGUI = $('.tab-cutoffs #powerMax');
	var thrustMinGUI = $('.tab-cutoffs #thrustMin');
	var thrustMaxGUI = $('.tab-cutoffs #thrustMax');
	var torqueMinGUI = $('.tab-cutoffs #torqueMin');
	var torqueMaxGUI = $('.tab-cutoffs #torqueMax');
	var rpmMinGUI = $('.tab-cutoffs #rpmMin');
	var rpmMaxGUI = $('.tab-cutoffs #rpmMax');
    var vibrationMinGUI = $('.tab-cutoffs #vibrationMin');
	var vibrationMaxGUI = $('.tab-cutoffs #vibrationMax');
    var tempMinGUI = [];
    var tempMaxGUI = [];
    for(var i=0; i<temperatures.length; i++){   
        var temp = temperatures[i];
        var id = temp.id;
        tempMinGUI[i] = $('.tab-cutoffs #temp' + (i+1) + 'Min');
        tempMaxGUI[i] = $('.tab-cutoffs #temp' + (i+1) + 'Max');
    }
    
	//min voltage
	voltageMinGUI.attr({
		step: SCALES.voltage,
		min: SYSTEM_LIMITS.voltageMin,
		max: USER_LIMITS.voltageMax
	});
	voltageMinGUI.val(USER_LIMITS.voltageMin);
	voltageMinGUI.change(function () {
		minLimitsGUI(voltageMinGUI, voltageMaxGUI);
	});
	//max voltage
	voltageMaxGUI.attr({
		step: SCALES.voltage,
		min: USER_LIMITS.voltageMin,
		max: SYSTEM_LIMITS.voltageMax
	});
	voltageMaxGUI.val(USER_LIMITS.voltageMax);
	voltageMaxGUI.change(function () {
		maxLimitsGUI(voltageMinGUI, voltageMaxGUI);
	});
	//system voltage
	$('.tab-cutoffs #systemVoltage').html(SYSTEM_LIMITS.voltageMin + ' / ' + SYSTEM_LIMITS.voltageMax);
	
    //min current
	currentMinGUI.attr({
		step: SCALES.current,
		min: SYSTEM_LIMITS.currentMin,
		max: USER_LIMITS.currentMax
	});
	currentMinGUI.val(USER_LIMITS.currentMin);
	currentMinGUI.change(function () {
		minLimitsGUI(currentMinGUI, currentMaxGUI);
	});
	//max current
	currentMaxGUI.attr({
		step: SCALES.current,
		min: USER_LIMITS.currentMin,
		max: SYSTEM_LIMITS.currentMax
	});
    function currentCableWarning(){
        if(USER_LIMITS.currentMax>40){
            $("img#cablesWarning").show();
        }else{
            $("img#cablesWarning").hide();
        }
    }
	currentMaxGUI.val(USER_LIMITS.currentMax);
    currentCableWarning();
	currentMaxGUI.change(function () {
		maxLimitsGUI(currentMinGUI, currentMaxGUI);
        currentCableWarning();
	});
	//system current
	$('.tab-cutoffs #systemCurrent').html(SYSTEM_LIMITS.currentMin + ' / ' + SYSTEM_LIMITS.currentMax);
	
    //min currentBurst
	currentBurstMinGUI.attr({
		step: SCALES.currentBurst,
		min: SYSTEM_LIMITS.currentBurstMin,
		max: USER_LIMITS.currentBurstMax
	});
	currentBurstMinGUI.val(USER_LIMITS.currentBurstMin);
	currentBurstMinGUI.change(function () {
		minLimitsGUI(currentBurstMinGUI, currentBurstMaxGUI);
	});
	//max currentBurst
	currentBurstMaxGUI.attr({
		step: SCALES.currentBurst,
		min: USER_LIMITS.currentBurstMin,
		max: SYSTEM_LIMITS.currentBurstMax
	});
	currentBurstMaxGUI.val(USER_LIMITS.currentBurstMax);
	currentBurstMaxGUI.change(function () {
		maxLimitsGUI(currentBurstMinGUI, currentBurstMaxGUI);
	});
	//system currentBurst
	$('.tab-cutoffs #systemCurrentBurst').html(SYSTEM_LIMITS.currentBurstMin + ' / ' + SYSTEM_LIMITS.currentBurstMax);
    
    //min power
	powerMinGUI.attr({
		step: SCALES.power,
		min: SYSTEM_LIMITS.powerMin,
		max: USER_LIMITS.powerMax
	});
	powerMinGUI.val(USER_LIMITS.powerMin);
	powerMinGUI.change(function () {
		minLimitsGUI(powerMinGUI, powerMaxGUI);
	});
	//max power
	powerMaxGUI.attr({
		step: SCALES.power,
		min: USER_LIMITS.powerMin,
		max: SYSTEM_LIMITS.powerMax
	});
	powerMaxGUI.val(USER_LIMITS.powerMax);
	powerMaxGUI.change(function () {
		maxLimitsGUI(powerMinGUI, powerMaxGUI);
	});
	//system power
	$('.tab-cutoffs #systemPower').html(SYSTEM_LIMITS.powerMin + ' / ' + SYSTEM_LIMITS.powerMax);   
    
    //min thrust
	thrustMinGUI.attr({
		step: convertScale('thrust', SCALES.thrust),
		min: UNITS.convertToDisplay('thrust', SYSTEM_LIMITS.thrustMin).toFixed(0),
		max: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMax).toFixed(1)
	});
	thrustMinGUI.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMin).toFixed(1));
	thrustMinGUI.change(function () {
		minLimitsGUI(thrustMinGUI, thrustMaxGUI);
	});
	//max thrust
	thrustMaxGUI.attr({
		step: convertScale('thrust', SCALES.thrust),
		min: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMin).toFixed(1),
		max: UNITS.convertToDisplay('thrust', SYSTEM_LIMITS.thrustMax).toFixed(0)
	});
	thrustMaxGUI.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMax).toFixed(1));
	thrustMaxGUI.change(function () {
		maxLimitsGUI(thrustMinGUI, thrustMaxGUI);
	});
	//system thrust
	$('.tab-cutoffs #systemThrust').html(UNITS.convertToDisplay('thrust', SYSTEM_LIMITS.thrustMin).toFixed(0) + ' / ' + UNITS.convertToDisplay('thrust', SYSTEM_LIMITS.thrustMax).toFixed(0));
	//min torque
	var torqueDecimals = ($.inArray(UNITS.display['torque'], ["lbfft", "kgfm"]) > -1) ? 2 : 0;
	torqueMinGUI.attr({
		step: convertScale('torque', SCALES.torque),
		min: UNITS.convertToDisplay('torque', SYSTEM_LIMITS.torqueMin).toFixed(torqueDecimals),
		max: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMax).toFixed(torqueDecimals)
	});
	torqueMinGUI.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMin).toFixed(torqueDecimals));
	torqueMinGUI.change(function () {
		minLimitsGUI(torqueMinGUI, torqueMaxGUI);
	});
	//max torque
	torqueMaxGUI.attr({
		step: convertScale('torque', SCALES.torque),
		min: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMin).toFixed(torqueDecimals),
		max: UNITS.convertToDisplay('torque', SYSTEM_LIMITS.torqueMax).toFixed(torqueDecimals)
	});
	torqueMaxGUI.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMax).toFixed(torqueDecimals));
	torqueMaxGUI.change(function () {
		maxLimitsGUI(torqueMinGUI, torqueMaxGUI);
	});
	//system torque
	$('.tab-cutoffs #systemTorque').html(UNITS.convertToDisplay('torque', SYSTEM_LIMITS.torqueMin).toFixed(torqueDecimals) + ' / ' + UNITS.convertToDisplay('torque', SYSTEM_LIMITS.torqueMax).toFixed(torqueDecimals));
	
    //min motorSpeed
	rpmMinGUI.attr({
		step: SCALES.motorSpeed,
		min: UNITS.convertToDisplay('motorSpeed', SYSTEM_LIMITS.rpmMin).toFixed(0),
		max: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMax).toFixed(0)
	});
	rpmMinGUI.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMin).toFixed(0));
	rpmMinGUI.change(function () {
		minLimitsGUI(rpmMinGUI, rpmMaxGUI);
	});
	//max motorSpeed
	rpmMaxGUI.attr({
		step: SCALES.motorSpeed,
		min: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMin).toFixed(0),
		max: UNITS.convertToDisplay('motorSpeed', SYSTEM_LIMITS.rpmMax).toFixed(0)
	});
	rpmMaxGUI.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMax).toFixed(0));
	rpmMaxGUI.change(function () {
		maxLimitsGUI(rpmMinGUI, rpmMaxGUI);
	});
	//system RPM
	$('.tab-cutoffs #systemRpm').html(UNITS.convertToDisplay('motorSpeed', SYSTEM_LIMITS.rpmMin).toFixed(0) + ' / ' + UNITS.convertToDisplay('motorSpeed', SYSTEM_LIMITS.rpmMax).toFixed(0) + ' <img src="images/question.png" alt="Pole Info" title="This value depends on the number of motor poles (or on the number of divisors if you use the optical rpm probe)." height="15" width="15">');
    
    //min vibration
	vibrationMinGUI.attr({
		step: SCALES.vibration,
		min: SYSTEM_LIMITS.vibrationMin,
		max: USER_LIMITS.vibrationMax
	});
	vibrationMinGUI.val(USER_LIMITS.vibrationMin);
	vibrationMinGUI.change(function () {
		minLimitsGUI(vibrationMinGUI, vibrationMaxGUI);
	});
	//max vibration
	vibrationMaxGUI.attr({
		step: SCALES.vibration,
		min: USER_LIMITS.vibrationMin,
		max: SYSTEM_LIMITS.vibrationMax
	});
	vibrationMaxGUI.val(USER_LIMITS.vibrationMax);
	vibrationMaxGUI.change(function () {
		maxLimitsGUI(vibrationMinGUI, vibrationMaxGUI);
	});
	//system vibration
    $('.tab-cutoffs #systemVibration').html(SYSTEM_LIMITS.vibrationMin + ' / ' + SYSTEM_LIMITS.vibrationMax);  
    
    //temperatures
    for(var i=0; i<temperatures.length; i++){   
        var temp = temperatures[i];
        var id = temp.id;
        
        function generateCallback(fct, i){
            var min = tempMinGUI[i];
            var max = tempMaxGUI[i];
            var result = function(){
                fct(min, max);
            }
            return result;
        }
        
        //min temp
        tempMinGUI[i].attr({
            step: SCALES.temperature,
            min: UNITS.convertToDisplay('temperature', SYSTEM_LIMITS.temperatureMin),
            max: UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMax[id])
        });
        tempMinGUI[i].val(UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMin[id]));
        tempMinGUI[i].change(generateCallback(minLimitsGUI, i));
        //max temp
        tempMaxGUI[i].attr({
            step: SCALES.temperature,
            min: UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMin[id]),
            max: UNITS.convertToDisplay('temperature', SYSTEM_LIMITS.temperatureMax)
        });
        tempMaxGUI[i].val(UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMax[id]));
        tempMaxGUI[i].change(generateCallback(maxLimitsGUI, i)); 
    }
    //system temp
    $('.tab-cutoffs #systemTemp').html(UNITS.convertToDisplay('temperature', SYSTEM_LIMITS.temperatureMin) + ' / ' + UNITS.convertToDisplay('temperature', SYSTEM_LIMITS.temperatureMax)); 
    
    //Reveal new rows in GUI for temperature probes
    for(var i=0; i<temperatures.length; i++){
        $('#cutoffRowTemp'+(i+1)).show();
    }

	//save user cutoff values
	$('.tab-cutoffs input').change(function () {
		USER_LIMITS.voltageMin = Number(voltageMinGUI.val());
		USER_LIMITS.voltageMax = Number(voltageMaxGUI.val());
		USER_LIMITS.currentMin = Number(currentMinGUI.val());
		USER_LIMITS.currentMax = Number(currentMaxGUI.val());
        USER_LIMITS.currentBurstMin = Number(currentBurstMinGUI.val());
		USER_LIMITS.currentBurstMax = Number(currentBurstMaxGUI.val());
        USER_LIMITS.powerMin = Number(powerMinGUI.val());
		USER_LIMITS.powerMax = Number(powerMaxGUI.val());
		USER_LIMITS.thrustMin = Number(UNITS.convertToWorking('thrust', thrustMinGUI.val()).toFixed(1));
		USER_LIMITS.thrustMax = Number(UNITS.convertToWorking('thrust', thrustMaxGUI.val()).toFixed(1));
		USER_LIMITS.torqueMin = Number(UNITS.convertToWorking('torque', torqueMinGUI.val()).toFixed(1));
		USER_LIMITS.torqueMax = Number(UNITS.convertToWorking('torque', torqueMaxGUI.val()).toFixed(1));
		USER_LIMITS.rpmMin = Number(UNITS.convertToWorking('motorSpeed', rpmMinGUI.val()).toFixed(0));
		USER_LIMITS.rpmMax = Number(UNITS.convertToWorking('motorSpeed', rpmMaxGUI.val()).toFixed(0));
        USER_LIMITS.vibrationMin = Number(vibrationMinGUI.val());
		USER_LIMITS.vibrationMax = Number(vibrationMaxGUI.val());
        //temperatures
        for(var i=0; i<temperatures.length; i++){   
            var temp = temperatures[i];
            var id = temp.id;
            USER_LIMITS.temperaturesMin[id] = Number(UNITS.convertToWorking('temperature',tempMinGUI[i].val()));
            USER_LIMITS.temperaturesMax[id] = Number(UNITS.convertToWorking('temperature',tempMaxGUI[i].val()));
        }

		chrome.storage.local.set({'USER_LIMITS': USER_LIMITS});
	});
}

TABS.cutoffs = {};
TABS.cutoffs.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'cutoffs') {
        GUI.active_tab = 'cutoffs';
        googleAnalytics.sendAppView('Cutoffs');
    }

    $('#tabContent').load("./tabs/cutoffs.html", function () {

        // translate to user-selected language
        localize();
        
        showBoardSpecific();
		
		refreshCutoffs();
        
        $("img#burstCurrentPopup").click(function(){				   
			$('#burstCurrentDialog').load("../burstcurrent.html", function (content) {
				$('#burstCurrentDialog').dialog({
					show: {
						effect: "blind",
						duration: 500
					},
					hide: {
						effect: "blind",
						duration: 500
					},
					title: "Current burst information",
					resizable: true,
					draggable: true,
					height: 600,
					width: 600,
					dialogClass: 'ui-dialog-osx',
					modal: true,
					open : function() {
					   $(this).parent().promise().done(function () {
					   	$(".ui-dialog-content").scrollTop(0);
					   	console.log("scrolling to top");
					   });
					},
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
				});
			});	
			googleAnalytics.sendEvent('BurstCurrentHelp', 'Click');	
		});
        
        // Setup "?" tooltips
        $("#limits > tbody > tr:nth-child(9) > td:nth-child(1) > img").tooltip({ show: { effect: "blind", duration: 100 } });
		$("#systemRpm > img").tooltip({ show: { effect: "blind", duration: 100 } });
        $("#cablesWarning").tooltip({ show: { effect: "blind", duration: 100 } });
        if (callback) callback();
    });
};

TABS.cutoffs.cleanup = function (callback) {
    if (callback) callback();
};

function minLimitsGUI(min, max) {
	var minVal = Number(min.val());
	var minMin = Number(min.attr('min'));
	var minMax = Number(min.attr('max'));
	if(minVal < minMin){
		max.attr('min', min.attr('min'));
		min.val(min.attr('min'));
	}
	else if(minVal > minMax){
		max.attr('min', min.attr('max'));
		min.val(min.attr('max'));
	}
	else
		max.attr('min', min.val());
}

function maxLimitsGUI(min, max) {
	var maxVal = Number(max.val());
	var maxMin = Number(max.attr('min'));
	var maxMax = Number(max.attr('max'));
	if(maxVal < maxMin){
		min.attr('max', max.attr('min'));
		max.val(max.attr('min'));
	}
	else if(maxVal > maxMax){
		min.attr('max', max.attr('max'));
		max.val(max.attr('max'));
	}
	else
		min.attr('max', max.val());
}

function convertScale(sensor, scale) {
	var convertedScale = UNITS.convertToDisplay(sensor, scale);
	var order = Math.floor(Math.log(convertedScale) / Math.LN10 + 0.000000001);
	var step = Math.pow(10, order);
    return step;
}