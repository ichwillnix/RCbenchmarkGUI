TABS.setup = {
    yaw_fix: 0.0
};

//TEMPERATURE PROBES

function getTempProbeLocation(id) {
    var location = CONFIG.temperatureProbes.value[id];
    var result = "";
    if(location !== undefined){
        result = location;
    }
    return result;
}

function setTempProbeLocation(id, location) {
    var value = CONFIG.temperatureProbes.value;
    value[id] = location;
    changeMemory(CONFIG.temperatureProbes, value);
}

//Creates the HTML
function htmlTempProbes() {
    //help
    $("#tempInfoButton").click(function(){
        $('#temperatureShowDialog').load("../temperatureprobes.html", function (content) {
            $('#temperatureShowDialog').dialog({
                show: {
                    effect: "blind",
                    duration: 500
                },
                hide: {
                    effect: "blind",
                    duration: 500
                },
                resizable: true,
                draggable: true,
                height: 600,
                width: 650,
                dialogClass: 'ui-dialog-osx',
                modal: true,
                open: function () {
                    $(this).parent().promise().done(function () {
                        $(".ui-dialog-content").scrollTop(0);
                    });
                },
                close: function(event, ui){
                    $(this).dialog('destroy');
                }
            });
        });
        googleAnalytics.sendEvent('TemperatureHelp', 'Click');
    })
    
    if(CONFIG.tempProbesQty>0){
        //Temperature probes found
        $("#no-temp-probes-display").hide();
        $("#temp-probes-display").show();

        //Add a line for each probe found
        var content = "";
        for (var i=0;i<CONFIG.tempProbesQty;i++){
            var id = SENSOR_DATA.temperature[i].id;
            
            content += '<tr><td><label id="';
            content += 'temp-probe-value-' + id; //HTML VALUE
            content += '"></label></td><td><input type="text" class="tempProbeLocation" id="';
            content += 'temp-probe-input-' + id; //HTML INPUT ID
            content += '" name="" placeholder="';
            content += id;
            content += '"></td></tr>';
        }
        $("#temp-probes-display > table").html(content);
        
        //Fill in the location values and create callbacks
        function createCallback(id, elem){
            return function(){
                console.log(id + ": " + $( elem ).val());
                setTempProbeLocation(id, $( elem ).val());
                TABS.sensors.initialize();
            }
        }
        for (var i=0;i<CONFIG.tempProbesQty;i++){
            var id = SENSOR_DATA.temperature[i].id;
            var location = getTempProbeLocation(id);
            var elem = '#temp-probe-input-' + id;
            $(elem).val(location);
            $(elem).change(createCallback(id, elem));
        }      
    }else{
        //No temperature probes
        $("#no-temp-probes-display").show();
        $("#temp-probes-display").hide();
    }  
}

//Refreshes the values
function refreshTempProbes() {
    for (var i=0;i<CONFIG.tempProbesQty;i++){
        var id = SENSOR_DATA.temperature[i].id;
        var valueHtml = '<b>' + id + ' (';
        valueHtml += SENSOR_DATA.temperature[i].value.toFixed(1);
        valueHtml += 'ºC):</b>';
        $('#temp-probe-value-' + id).html(valueHtml);
    }
}

function setFirmwareLabels() {
	//LOCAL VARIABLE
	var firmwareUpdateBtn = $('#setup-firmware-update-button');
	//Firmware flasher button text
	var firmwareVersion = CONFIGURATOR.firmwareVersionAccepted;
	firmwareUpdateBtn.find('a.flash-firmware').html(chrome.i18n.getMessage('firmwareButton',[firmwareVersion]));

	if(CONFIG.firmwareVersion === 'unknown'){
		$('.flashResult').html(chrome.i18n.getMessage('firmwareUnknown'));
	}else{
		if(CONFIG.firmwareVersion === firmwareVersion){
			$('.flashResult').html(chrome.i18n.getMessage('firmwareOk',[firmwareVersion]));
		}else{
			$('.flashResult').html(chrome.i18n.getMessage('firmwareVersionNotSupported',[CONFIG.firmwareVersion,firmwareVersion]));
		}
	}
}

//reveals the debug tab (function also called in main.js)
function activateDevelopperMode(){
	if(!activateDevelopperMode.activated){
		$('li.tab_debug').show();
		$(".tab-sensors .info .checkboxes input[name='debug']").parent().show();
		$(".tab-sensors .info .checkboxes input[name='debug']").prop('checked', false);
		$(".tab-sensors .info .checkboxes input[name='loadcells']").parent().show();
		$(".tab-sensors .info .checkboxes input[name='loadcells']").prop('checked', false);
		$('#data-pane').css('top', parseInt($('#data-pane').css('top')) + 26);
		$('.show_plot').css('bottom', parseInt($('.show_plot').css('bottom')) + 0);
		activateDevelopperMode.activated = true;
	}
	$('div.plots').css('top', parseInt($('div.plots').css('top'))+0);
}

TABS.setup.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'setup') {
        GUI.active_tab = 'setup';
        googleAnalytics.sendAppView('Setup');
    }

    function load_html() {
        $('#tabContent').load("./tabs/setup.html", process_html);
    }
    
    load_html();

    function process_html() {
        // translate to user-selected language
        localize();
        
        // show/hide based on board version
        showBoardSpecific();
	
		// units dropdown management
		var dropdown = '';
		for (var choice in UNITS.choices) { 
			for (var i = 0; i < UNITS.choices[choice].length; i++) {
				dropdown += '<option value="' + UNITS.choices[choice][i] + '">' + UNITS.getUnitFullText(UNITS.choices[choice][i]) + '</option>\n';
			}
			$('.tab-setup select[name=' + choice + ']').html(dropdown);
			$('.tab-setup select[name=' + choice + ']').val(UNITS.display[choice]);
			dropdown = '';
		}
		$('.tab-setup select.dropdown').change(function () { //save user units display
			var self = this;
			var choice = this.name;
			UNITS.display[choice] = this.value;
			
			chrome.storage.local.set({'UNITS_DISPLAY': UNITS.display});
			TABS.sensors.initialize();

			logSample.newLog();
			googleAnalytics.sendEvent('ChangedUnits', 'Click');
		});
		
		// calibration wizard management
		function showCalibWizard(wizardId){			
			$('#showCalibWizard').load("tabs/calib_wizard.html", function () {
                var popupTitle = wizardId.capitalizeFirstLetter() + " Calibration Wizard";
                $('#showCalibWizard').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    title: popupTitle,
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 600,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
                calib_wizard(wizardId);
            });
		}
		$('#content .tab-setup a.calibrateDyn').click(function () {
            showCalibWizard('torque');
        });
		$('#content .tab-setup a.calibrateDynThrust').click(function () {
            showCalibWizard('thrust');
        });
		
		// developer mode management
		$('input.developer_mode').prop('checked', CONFIG.developper_mode);
		$('input.developer_mode').change(function () {
			CONFIG.developper_mode = $('input.developer_mode').prop( "checked" );
			googleAnalytics.sendEvent('DebugMode','Using', CONFIG.developper_mode);
			chrome.storage.local.set({'DEBUG_MODE': CONFIG.developper_mode});
			logSample.newLog(); //force a new log because header will be different
			if(CONFIG.developper_mode) {
				activateDevelopperMode();
			} else {
				$('li.tab_debug').hide();
				$(".tab-sensors .info .checkboxes input[name='debug']").parent().hide();
				$(".tab-sensors .info .checkboxes input[name='debug']").prop('checked', false);
				$('.tab-sensors .plots .debug').hide();
				$(".tab-sensors .info .checkboxes input[name='loadcells']").parent().hide();
				$(".tab-sensors .info .checkboxes input[name='loadcells']").prop('checked', false);
				$('.tab-sensors .plots .loadcells').hide();
				$('#data-pane').css('top', parseInt($('#data-pane').css('top')) - 26);
				$('.show_plot').css('bottom', parseInt($('.show_plot').css('bottom')) - 26);
				$('div.plots').css('top', parseInt($('div.plots').css('top'))-20);
				activateDevelopperMode.activated = false;
			}
		});

		//Scripting mode management
		$('input.scripting_mode').prop('checked', CONFIG.scripting_mode);
		$('input.scripting_mode').change(function () {
			if($('input.scripting_mode').prop( "checked" )){
				$('input.scripting_mode').prop('checked',false);
				$('#showScriptDialog').load("../scriptWarning.html", function () {
					var dialog = $('#showScriptDialog').dialog({
						show: {
							effect: "blind",
							duration: 500
						},
						hide: {
							effect: "blind",
							duration: 500
						},
						title: "Scripting Mode Warning",
						resizable: true,
						draggable: true,
						height: 315,
						width: 600,
						dialogClass: 'ui-dialog-osx',
						modal: true,
						open : function() {
						   $(this).parent().promise().done(function () {
							$(".ui-dialog-content").scrollTop(0);
						   });
						},
                        close: function(event, ui){
                            $(this).dialog('destroy');
                        }
					});

					$('a.yes-button').click(function(){
						$(dialog).dialog('close');
						CONFIG.scripting_mode = true;
						loadScriptingTab();
						$('input.scripting_mode').prop('checked',true);
						googleAnalytics.sendEvent('ScriptingMode','Using', CONFIG.scripting_mode);
						chrome.storage.local.set({'SCRIPTING_MODE': CONFIG.scripting_mode});
					});

					$('a.no-button').click(function(){
						$(dialog).dialog('close');
						CONFIG.scripting_mode = false;
						loadScriptingTab();
						chrome.storage.local.set({'SCRIPTING_MODE': CONFIG.scripting_mode});
					});
				});
			}else{
				CONFIG.scripting_mode = false;
				loadScriptingTab();
				chrome.storage.local.set({'SCRIPTING_MODE': CONFIG.scripting_mode});
			}
			
		});
		
		//Change working directory
		$('#setup-working-directory-button').find('.button-directory').on('click', function(){
            //Opens window to choose a directory and save it address
            chrome.fileSystem.chooseEntry({type: 'openDirectory'}, function(entry) {
				var homeDirectory = chrome.fileSystem.retainEntry(entry);
                chrome.storage.local.set({'logDirectory': homeDirectory}); 
				updateWorkingDirectory ();
            });   
            googleAnalytics.sendEvent('SetDirectory','Click');    
        });
		
		//Working directory
		updateWorkingDirectory ();

		//Firmware labels
		setFirmwareLabels();

		//Flash the firmware when the button is pressed
        var firmwareVersion = CONFIGURATOR.firmwareVersionAccepted;
        var textdots = 0;

        //Setup Flash Firmware Button Event
		$('#setup-firmware-update-button').find('.flash-firmware').on('click', function(){
        	if(!GUI.firmware_is_flashing){
				GUI.firmware_is_flashing = true;
				
				$('#setup-firmware-update-button').find('a.flash-firmware').html(chrome.i18n.getMessage('firmwareFlashing'));
				$('.flashResult').html(chrome.i18n.getMessage('firmwarePleaseWait'));

				var reconnect = false;
				if(GUI.connected_to != false){
					reconnect = true;
					connection(); //disconnect the board first
				}

				// Flasher timout error
				GUI.interval_add('flashTimout', function(){
					flashError();
				}, 20000,false); //flashing firmware takes about 15 seconds.

				//Show animation
				GUI.interval_add('flashfirmware', function(){
					var dots = '';
					for (var i = 0; i < textdots; i++) { 
    					dots += '.';
					}
					textdots++;
					if(textdots>10) textdots = 0;
					$('.flashResult').html(chrome.i18n.getMessage('firmwarePleaseWait') + '<b>' + dots + '</b>');
				}, 250, true);

				//Get the firmware from the file
				$.get( "/resources/firmware/"+firmwareVersion+".hex", function( data ){ 
					var params = {};
					params.data = data;
					params.selected_port = String($('div#port-picker #port').val());

					//Upload the hex file to the board
					stk500.upload(params, function(error){
						if( typeof error === 'undefined' || error === null ){
							flashOk();
							googleAnalytics.sendEvent('FirmwareFlash', 'Click');
						}else{
							console.log("error var:" + error);
							flashError();
						}						
					});
				});

				function flashError(){
					//LOCAL VARIABLES//
					var firmwareUpdateBtn = $('#setup-firmware-update-button');
					GUI.firmware_is_flashing = false;
					GUI.interval_remove('flashfirmware');
					GUI.interval_remove('flashTimout');
					$('.flashResult').html(chrome.i18n.getMessage('firmwareFlashError'));
					firmwareUpdateBtn.find('a.flash-firmware').html(chrome.i18n.getMessage('firmwareButton',[firmwareVersion]));
				}
				
				function flashOk(){
					//LOCAL VARIABLES//
					var firmwareUpdateBtn = $('#setup-firmware-update-button');
					if(reconnect){
							connection(); //reconnect the board if it was connected
					}else{
						GUI.firmware_is_flashing = false;
					}
					GUI.interval_remove('flashfirmware');
					GUI.interval_remove('flashTimout');
					$('.flashResult').html(chrome.i18n.getMessage('firmwareFlashOk',[firmwareVersion]));
					firmwareUpdateBtn.find('a.flash-firmware').html(chrome.i18n.getMessage('firmwareButton',[firmwareVersion]));
				}
        	}
        });
        ////////  Pole input initialization  
        function rpmDivisorCode(identifierHTML, numberOfDivisor, blindTitle, pathHTML, divisor) {
            function setInitialPoleNumber(value) {
                $('#' + identifierHTML + 'Input').val(value);
            }
            initialMemoryLoad(setInitialPoleNumber, numberOfDivisor);

            $('#' + identifierHTML + 'Input').focusout(function () {
                if (!isNaN($('#' + identifierHTML + 'Input').val())) {
                    changeMemory(numberOfDivisor, $('#' + identifierHTML + 'Input').val());
                                    console.log(numberOfDivisor);
                    refreshCutoffs();
                }
            });
            
            $('#mainRPMInfoButton').click(function () {
                $('#divisorShowDialog').load("../mainRPMsensor.html", function (content) {
                    $('#divisorShowDialog').dialog({
                        show: {
                            effect: "blind",
                            duration: 500
                        },
                        hide: {
                            effect: "blind",
                            duration: 500
                        },
                        title: "Main speed sensor",
                        resizable: true,
                        draggable: true,
                        height: 170,
                        width: 650,
                        dialogClass: 'ui-dialog-osx',
                        modal: true,
                        open: function () {
                            $(this).parent().promise().done(function () {
                                $(".ui-dialog-content").scrollTop(0);
                            });
                        },
                        close: function(event, ui){
                            $(this).dialog('destroy');
                        }
                    });
                });
                googleAnalytics.sendEvent(identifierHTML + 'Help', 'Click');
            });

            $('#' + identifierHTML + 'InfoButton').click(function () {
                $('#divisorShowDialog').load(pathHTML, function (content) {
                    $('#divisorShowDialog').dialog({
                        show: {
                            effect: "blind",
                            duration: 500
                        },
                        hide: {
                            effect: "blind",
                            duration: 500
                        },
                        title: blindTitle,
                        resizable: true,
                        draggable: true,
                        height: 600,
                        width: 650,
                        dialogClass: 'ui-dialog-osx',
                        modal: true,
                        open: function () {
                            $(this).parent().promise().done(function () {
                                $(".ui-dialog-content").scrollTop(0);
                            });
                        },
                        close: function(event, ui){
                            $(this).dialog('destroy');
                        }
                    });
                });
                googleAnalytics.sendEvent(identifierHTML + 'Help', 'Click');
            });
            $('#' + identifierHTML + 'Input').change(function () {
                if ($('#' + identifierHTML + 'Input').val() % divisor == 1) {
                    var value = $('#' + identifierHTML + 'Input').val();
                    value++;
                    $('#' + identifierHTML + 'Input').val(value);
                }
                if (!isNaN($('#' + identifierHTML + 'Input').val())) {
                    changeMemory(numberOfDivisor, $('#' + identifierHTML + 'Input').val());
                    googleAnalytics.sendEvent('' + identifierHTML + 'Change', 'Click');
                    refreshCutoffs();
                }
            });

            $('#' + identifierHTML + 'Setting').keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        }
        rpmDivisorCode('pole', CONFIG.numberOfMotorPoles, 'Motor Pole Information', "../motorpoles.html", 2);
        rpmDivisorCode('optical', CONFIG.numberOfOpticalTape, 'Optical Sensor Information', "../opticaltapes.html", 1);

        

        // Electrical RPM Active Checkbox 
        //// Initialization

        $("#RPMActiveButtonElectrical").prop('checked', CONFIG.electricalRPMActive.value);
        

        //// On Click
        $("#RPMActiveButtonElectrical").change(function () {
            // Do not allow to deactivate if is the main RPM sensor.
            if (CONFIG.mainRPMsensor.value == "electrical") {
                if (this.checked == false){
                    $("#RPMActiveButtonElectrical").prop('checked', true);
                }   
            }
            else {
                changeMemory(CONFIG.electricalRPMActive, this.checked);
            }
        });
        //

        // Optical RPM Active Checkbox 
        //// Initialization

        $("#RPMActiveButtonOptical").prop('checked', CONFIG.opticalRPMActive.value);

        //// On Click
        $("#RPMActiveButtonOptical").change(function () {
            // Do not allow to deactivate if is the main RPM sensor.
            if (CONFIG.mainRPMsensor.value == "optical") {
                if (this.checked == false){
                    $("#RPMActiveButtonOptical").prop('checked', true);
                }
            }
            else {
                changeMemory(CONFIG.opticalRPMActive, this.checked);
            }
        });
        // 
        // The main RPM sensor is always active
        function setMainRPMsensor(mainSensor) { 
            switch (mainSensor) {
                case "electrical":
                    // Activate RPM sensor
                    $('input[type=radio][value=electrical]').prop("checked", true);
                    changeMemory(CONFIG.mainRPMsensor, mainSensor);
                    // Check main RPM sensor
                    $("#RPMActiveButtonElectrical").prop('checked', true);
                    changeMemory(CONFIG.electricalRPMActive, true);
                    break;
                case "optical":
                    // Activate RPM sensor
                    $('input[type=radio][value=optical]').prop("checked", true);
                    changeMemory(CONFIG.mainRPMsensor, mainSensor);
                    $("#RPMActiveButtonOptical").prop('checked', true);
                    changeMemory(CONFIG.opticalRPMActive, true);
                    break;
                default:
                    throw "Invalid RPM sensor"
            }
        };
        // Radio Main RPM sensor
        //// Initialization
        switch (CONFIG.mainRPMsensor.value) {
             case "electrical":
                $('input[type=radio][value=electrical]').prop("checked", true);
                break;
            case "optical":
                $('input[type=radio][value=optical]').prop("checked", true);
                break;
            default:
                throw "Invalid RPM sensor"
        }
        //// On Click
        $('input[type=radio][name=RPMMainButton]').change(function () {
            setMainRPMsensor(this.value);
        });
        
        
        ////////  Temperature Probes
        htmlTempProbes();
        refreshTempProbes();
        
        
        ////////////

		// if tracking is enabled, check the statistics checkbox
        if (googleAnalyticsConfig.isTrackingPermitted() !== false) {
            $('.googleAnalytics').prop('checked', true);
            //sets true, so that 1st user clicks its allowing tracking
            googleAnalyticsConfig.setTrackingPermitted(true);
        }

        $('.googleAnalytics').change(function () {
            var check = $(this).is(':checked');
            googleAnalytics.sendEvent('Settings', 'GoogleAnalytics', check);
            googleAnalyticsConfig.setTrackingPermitted(check);
        });

        if (callback) callback();
    } // process html
};

function updateWorkingDirectory () {
	// var workingDirectory = '<label>Working Directory:</label></br>';	
	var workingDirectory = '';	
	chrome.storage.local.get('logDirectory', function (result) {
		//verify if there is a default directory registered, if not user will have to set now 
		if(result.logDirectory === undefined){
			workingDirectory += '<label>Please choose where log files will be saved.</label>';
			$('.tab-setup #working-directory').html(workingDirectory);   
		}
		else{
			chrome.fileSystem.restoreEntry(result.logDirectory, function (entry) {
				chrome.fileSystem.getDisplayPath(entry, function(path) {
					workingDirectory += '<label>' + path + '</label>';
					$('.tab-setup #working-directory').html(workingDirectory);
				});
				CONFIG.workingDirectory = entry;
			});
		}
	});

	logSample.updateLogInfo();
};

TABS.setup.cleanup = function (callback) {
    $(window).off('resize', this.resize3D);

    if (callback) callback();
};