'use strict';

TABS.utilities = {};

//Called by the scripting function as well
function readOhmmeter(callback){
	googleAnalytics.sendEvent('Feature', 'Using', 'Ohmmeter');

	//poll the status until good
	GUI.interval_add('ohmmeter', function () {
		MSP.send_message(MSP_codes.MSP_GETOHM, false, false, function() {
			if(SENSOR_DATA.ohmStatus >= 2){
				GUI.interval_remove('ohmmeter');
				if(callback)callback(SENSOR_DATA.ohmReading);
			}
		});
	}, 100, true);
}

// This function will add the html for the pressure sensor if it is available.
function htmlPressureSensor()
{
    if(DATA.isPressureSensorAvailable()){
        $("#no-airspeed-probes-display").hide();
        $("#airspeed-probes-display").show();       
    }
    else {
        $("#no-airspeed-probes-display").show();
        $("#airspeed-probes-display").hide();
    }
}

TABS.utilities.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'utilities') {
        GUI.active_tab = 'utilities';
        googleAnalytics.sendAppView('Utilities');
    }

    $('#tabContent').load("./tabs/utilities.html", function () {

        // translate to user-selected language
        localize();
        
        showBoardSpecific();
        htmlPressureSensor();
        
        // handle the reverse checkboxes
        $(".tab-utilities .section .reverse_chk #reverse_thrust").prop( "checked", CONFIG.reverseThrust.value);
        $(".tab-utilities .section .reverse_chk #reverse_torque").prop( "checked", CONFIG.reverseTorque.value);
        $(".tab-utilities .section .reverse_chk #reverse_thrust").change(function(){
            var checked = $(this).is(":checked");
            changeMemory(CONFIG.reverseThrust, checked);
            googleAnalytics.sendEvent('ReverseThrust', 'Value', checked);
        });
        $(".tab-utilities .section .reverse_chk #reverse_torque").change(function(){
            var checked = $(this).is(":checked");
            changeMemory(CONFIG.reverseTorque, checked);
            googleAnalytics.sendEvent('ReverseTorque', 'Value', checked);
        });
        
        // handle CSV notes
        $(".tab-utilities #csv-notes-text").val(CONFIG.csvNotes.value);
        if(CONFIG.csvNotes.value && CONFIG.csvNotes.value !== ''){
            console.log("Loaded: ");
            console.log(CONFIG.csvNotes.value);
            googleAnalytics.sendEvent('Feature', 'Using', 'CSV-Notes');
        }
        $(".tab-utilities #csv-notes-text").on('input',function(){
            var notes = $(".tab-utilities #csv-notes-text").val();
            changeMemory(CONFIG.csvNotes, notes);
        });
        $(".tab-utilities #csv-notes-tooltip").tooltip({ show: { effect: "blind", duration: 100 } });
		
		// function to execute when GetOhm button is clicked
		var getOhmButton = $('#utilities-read-ohm-button .get-ohm-button');
		getOhmButton.on('click', function(){
			getOhmButton.text(chrome.i18n.getMessage('sensorsButtonGettingOhm'));
			readOhmmeter(function(value){
				var displayValue = value.toPrecision(4).toString() + ' Ohm';
				if(SENSOR_DATA.ohmStatus == 3){
					displayValue = '~' + displayValue;
				}
				if(SENSOR_DATA.ohmStatus == 4){
					displayValue = '>' + displayValue;
				}
				//Add a message class to change the size of the button to 
				//display the entire text
				getOhmButton.addClass('message');
				getOhmButton.text('Read again. Value: ' + displayValue); 
			});
		});
		
		//calibration
		function showCalibWizard(wizardId){            
			$('#showCalibWizard').load("tabs/calib_wizard.html", function () {
                var popupTitle = wizardId.capitalizeFirstLetter() + " Calibration Wizard";
                $('#showCalibWizard').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    title: popupTitle,
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 600,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
                calib_wizard(wizardId);
            });
		}
		//$('#content .tab-utilities a.calibrateDyn').click(function () {
		$('#calibrate-torque-weight-button .button').on('click', function(){
			googleAnalytics.sendEvent('CalibTorque', 'Click');
            showCalibWizard('torque');
        });
        
		//$('#content .tab-utilities a.calibrateDynThrust').click(function () {
		$('#calibrate-thrust-button .button').on('click', function(){
			googleAnalytics.sendEvent('CalibThrust', 'Click');
            showCalibWizard('thrust');
        });
		
        
        $(".pitotInfoButton").click(function(){
            console.log(2);
            $('#pitotShowDialog').load("../pressureSensor.html", function (content) {
                $('#pitotShowDialog').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 650,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    open: function () {
                        $(this).parent().promise().done(function () {
                            $(".ui-dialog-content").scrollTop(0);
                        });
                    },
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
            });
            googleAnalytics.sendEvent('PitotHelp', 'Click');
        })
        
        
        
		handleCalibration();
        
        // Output min and max section
        
        
        var ESCMinGUI  = $('.tab-utilities #ESCMin');
        var ESCMaxGUI = $('.tab-utilities #ESCMax');
        var ESCCutoffGUI = $('.tab-utilities #ESCCutoff');
        var servo1MinGUI = $('.tab-utilities #servo1Min');
        var servo1MaxGUI = $('.tab-utilities #servo1Max');
        var servo2MinGUI = $('.tab-utilities #servo2Min');
        var servo2MaxGUI = $('.tab-utilities #servo2Max');
        var servo3MinGUI = $('.tab-utilities #servo3Min');
        var servo3MaxGUI = $('.tab-utilities #servo3Max');
        
        var minPWMTime = SYSTEM_LIMITS.PWMTimeMin;
        var maxPWMTime = SYSTEM_LIMITS.PWMTimeMax;
        
        function updateESCCutoff() {
            if  ( Number(ESCMinGUI.val()) >  Number(ESCCutoffGUI.val()) ) {
                ESCCutoffGUI.val(ESCMinGUI.val());
            }  
            if  ( Number(ESCCutoffGUI.val()) >  Number(ESCMaxGUI.val()) ) {
                ESCCutoffGUI.val(ESCMaxGUI.val());
            }
            changeMemory(CONFIG.ESCCutoff, Number(ESCCutoffGUI.val()))
        }
        ESCCutoffGUI.attr({
            step: 1,
            min: minPWMTime,
            max: maxPWMTime
        });
        ESCCutoffGUI.val(CONFIG.ESCCutoff.value);
        ESCCutoffGUI.change(updateESCCutoff);
         
        
        function outputRangeGui(minGUI, maxGUI, isMin, savedValue) {
            var currentGUI;
            if (isMin) {
                currentGUI = minGUI;
            }
            else {
                currentGUI = maxGUI;
            }
            currentGUI.attr({
                step: 1,
                min: minPWMTime,
                max: maxPWMTime
            });
            currentGUI.val(savedValue.value);
            currentGUI.change(function () {
                if (isMin) {
                    if  ( Number(minGUI.val()) >  Number(maxGUI.val()) ) {
                        minGUI.val(maxGUI.val());
                    }
                    minLimitsGUI(minGUI, maxGUI);
                }
                else {
                    if  ( Number(minGUI.val()) >  Number(maxGUI.val()) ) {
                        maxGUI.val(minGUI.val());
                    }
                    maxLimitsGUI(minGUI, maxGUI);
                }
                changeMemory(savedValue, Number(currentGUI.val()))
                updateESCCutoff();
            });
        }
        outputRangeGui(ESCMinGUI, ESCMaxGUI, true, CONFIG.ESCMin);
        outputRangeGui(ESCMinGUI, ESCMaxGUI, false, CONFIG.ESCMax);
        outputRangeGui(servo1MinGUI, servo1MaxGUI, true, CONFIG.servo1Min);
        outputRangeGui(servo1MinGUI, servo1MaxGUI, false, CONFIG.servo1Max);     
        outputRangeGui(servo2MinGUI, servo2MaxGUI, true, CONFIG.servo2Min);
        outputRangeGui(servo2MinGUI, servo2MaxGUI, false, CONFIG.servo2Max);
        outputRangeGui(servo3MinGUI, servo3MaxGUI, true, CONFIG.servo3Min);
        outputRangeGui(servo3MinGUI, servo3MaxGUI, false, CONFIG.servo3Max);
        
        
        // The following section handles the change of value in the interface.
        
        function bindValue2Jq (jqHandle, memLocation, displayPrecision, minVal, maxVal) {
            // Jquery handle calibration coefficient
            // Set GUI value initial load
            jqHandle.val(parseFloat(memLocation.value).toFixed(displayPrecision));
            // If there is a change in the GUI        
            function changeValueInMem() {
                var val = jqHandle.val();
                val = Math.min(Math.max(minVal, val), maxVal);
                jqHandle.val(val);
                changeMemory(memLocation, val);
            }
            jqHandle.change( changeValueInMem );
        }
        
        bindValue2Jq ($('.tab-utilities #calibrationCoefficient'), CONFIG.pressureCal, 3, 0.01, 2);  
        bindValue2Jq ($('.tab-utilities #atmPressure'), CONFIG.atmPressure, 0, 85000, 106500);
        
		
        
        // ESC Cutoff
       
        updateESCCutoff();
        
		if (callback) callback();
	});
};

TABS.utilities.cleanup = function (callback) {
    if (callback) callback();
};