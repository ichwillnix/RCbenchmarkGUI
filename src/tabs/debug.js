'use strict';

TABS.debug = {};
TABS.debug.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'debug') {
        GUI.active_tab = 'debug';
        googleAnalytics.sendAppView('Debug');
    }

    $('#tabContent').load("./tabs/debug.html", function () {
        // translate to user-selected language
        localize();
        
        // save an updated system limit
        function saveSystemLimits(){
            /*FOR REFERENCE var SYSTEM_LIMITS = {
                voltageMin: 0,		    //[V]
                voltageMax: 35,			//[V]
                currentMin: 0,			//[A]
                currentMax: 40,			//[A]
                currentBurstMin: 0,		//[A]
                currentBurstMax: 50,	//[A]
                powerMin: 0,			//[W]
                powerMax: 1400,			//[W]
                rpmMin: 0,
                rpmMax: 0, //calculated upon init
                thrustMin: -3,			//[kg]
                thrustMax: 3,			//[kg]
                torqueMin: -2,			//[Nm]
                torqueMax: 2,			//[Nm]
                vibrationMin: 0,			//[g]
                vibrationMax: 6,			//[g]
            };*/
		  chrome.storage.local.set({'SYSTEM_LIMITS': SYSTEM_LIMITS});
          refreshCutoffs();
        }
        
        /* Setup hack codes
           - Hack codes are only shared by email upon customer request. 
           - Use the lowest increase that will satisfy the customer needs.
           - If you are getting these codes by reading this source code 
             or from anywhere else, do so AT YOUR OWN RISKS!
        */
        $('.tab-debug #debugHack').on( "change", function applyHack(){
            var pass = "";
            var fail = "";
            switch($(this).val()) {
                //**** RESET ****
                //Reset all hack codes
                case 'reset':
                    SYSTEM_LIMITS = $.extend(true, {}, ORIGINAL_SYSTEM_LIMITS);
                    saveSystemLimits();
                    pass = "Removed all hack codes.";
                    break;   
                
                //**** THRUST ****
                //Increase thrust limit to 5kg
                case 'stPhv':
                    SYSTEM_LIMITS.thrustMax = 5;
                    SYSTEM_LIMITS.thrustMin = -5;
                    saveSystemLimits();
                    pass = "Changed maximum thrust to 5kg.";
                    break;
                //Increase thrust limit to 10kg
                case 'bPhNN':
                    SYSTEM_LIMITS.thrustMax = 10;
                    SYSTEM_LIMITS.thrustMin = -10;
                    saveSystemLimits();
                    pass = "Changed maximum thrust to 10kg.";
                    break;
                    
                //**** TORQUE ****
                //Increase torque limit to 4Nm
                case 'HCsFp':
                    SYSTEM_LIMITS.torqueMax = 4;
                    SYSTEM_LIMITS.torqueMin = -4;
                    saveSystemLimits();
                    pass = "Changed maximum torque to 4Nm.";
                    break;
                //Increase torque limit to 10Nm
                case '9D7fT':
                    SYSTEM_LIMITS.torqueMax = 10;
                    SYSTEM_LIMITS.torqueMin = -10;
                    saveSystemLimits();
                    pass = "Changed maximum torque to 10Nm.";
                    break;
                    
                //**** CALIBRATION ****
                //Calibration always passes (for using custom load cells)
                case 'kGL7Lj':
                    SYSTEM_LIMITS.calibrationAlwaysPass = true;
                    saveSystemLimits();
                    pass = "Calibration will now always pass.";
                    break;
                    
                //**** MECHANICAL RPM ****
                //Increase rpm limit to 500000 erpm
                case 'Qux2e':
                    SYSTEM_LIMITS.erpmMax = 500000;
                    saveSystemLimits();
                    pass = "Max eRPM now 500000. RPM = 2 x eRPM / poles";
                    break;
                function checkBurstCurrent() {
                 if (SYSTEM_LIMITS.currentBurstMax < SYSTEM_LIMITS.currentMax){
                        SYSTEM_LIMITS.currentBurstMax = SYSTEM_LIMITS.currentMax;
                    }                   
                }    
                //**** CONTINUOUS CURRENT ****
                //Increase continuous current to 60A
                case 'yNx42':
                    SYSTEM_LIMITS.currentMin = -60;
                    SYSTEM_LIMITS.currentMax = 60;
                    checkBurstCurrent()
                    saveSystemLimits();
                    pass = "Changed continuous current to 60A";
                    break;
                //Increase continuous current to 70A
                case 'CgADR':
                    SYSTEM_LIMITS.currentMin = -70;
                    SYSTEM_LIMITS.currentMax = 70;
                    checkBurstCurrent()
                    saveSystemLimits();
                    pass = "Changed continuous current to 70A";
                    break;
                //Increase continuous current to 100A
                case 'AhJus':
                    SYSTEM_LIMITS.currentMin = -100;
                    SYSTEM_LIMITS.currentMax = 100;
                    checkBurstCurrent()
                    saveSystemLimits();
                    pass = "Changed continuous current to 100A";
                    break;
                //Increase continuous current to 150A
                case 'i43Ck':
                    SYSTEM_LIMITS.currentMin = -150;
                    SYSTEM_LIMITS.currentMax = 150;
                    checkBurstCurrent()
                    saveSystemLimits();
                    pass = "Changed continuous current to 150A";
                    break;
                //Increase continuous current to 200A -> DO NOT GO ABOVE THIS BEFORE HACKING THE FIRMWARE!
                case 'EZ3ir':
                    SYSTEM_LIMITS.currentMin = -200;
                    SYSTEM_LIMITS.currentMax = 200;
                    checkBurstCurrent()
                    saveSystemLimits();
                    pass = "Changed continuous current to 200A";
                    break;
                    
                //**** BURST CURRENT ****
                //Increase burst current to 80A
                case 'SfWpP':
                    SYSTEM_LIMITS.currentBurstMin = -80;
                    SYSTEM_LIMITS.currentBurstMax = 80;
                    saveSystemLimits();
                    pass = "Changed burst current to 80A";
                    break;
                //Increase burst current to 100A
                case 'Tj4Qt':
                    SYSTEM_LIMITS.currentBurstMin = -100;
                    SYSTEM_LIMITS.currentBurstMax = 100;
                    saveSystemLimits();
                    pass = "Changed burst current to 100A";
                    break;
                //Increase burst current to 150A
                case 'p4fWU':
                    SYSTEM_LIMITS.currentBurstMin = -150;
                    SYSTEM_LIMITS.currentBurstMax = 150;
                    saveSystemLimits();
                    pass = "Changed burst current to 150A";
                    break;
                //Increase burst current to 200A -> DO NOT GO ABOVE THIS BEFORE HACKING THE FIRMWARE!
                case 'xzQKh':
                    SYSTEM_LIMITS.currentBurstMin = -200;
                    SYSTEM_LIMITS.currentBurstMax = 200;
                    saveSystemLimits();
                    pass = "Changed burst current to 200A";
                    break;
                    
                //**** VOLTAGE ****
                //Increase voltage to 45V
                case 'ur5bK':
                    SYSTEM_LIMITS.voltageMax = 45;
                    saveSystemLimits();
                    pass = "Changed max voltage to 45V";
                    break;
                //Increase voltage to 55V -> DO NOT GO ABOVE THIS BEFORE HACKING THE FIRMWARE!
                case 'aGNmg':
                    SYSTEM_LIMITS.voltageMax = 55;
                    saveSystemLimits();
                    pass = "Changed max voltage to 55V";
                    break;
                    
                //**** POWER ****
                //Increase power to 3kW
                case 'QrB2y':
                    SYSTEM_LIMITS.powerMax = 3000;
                    saveSystemLimits();
                    pass = "Changed max power to 3kW";
                    break;
                //Increase power to 6kW
                case 'Fz76d':
                    SYSTEM_LIMITS.powerMax = 6000;
                    saveSystemLimits();
                    pass = "Changed max power to 6kW";
                    break;
                //Increase power to 10kW
                case 'ijDrw':
                    SYSTEM_LIMITS.powerMax = 10000;
                    saveSystemLimits();
                    pass = "Changed max power to 10kW";
                    break;
                    
                //**** VIBRATION ****
                //Increase vibration to 10
                case 'rndak':
                    SYSTEM_LIMITS.vibrationMax = 10;
                    saveSystemLimits();
                    pass = "Changed vibration limit to 10";
                    break;
                    
                default:
                    fail = "Error: unknown hack code";
            }
            $('.tab-debug #redHack').html(fail);
            $('.tab-debug #greenHack').html(pass);
        } );
	
        // load changelog content
		GUI.refresh_log();
        $('div.changelog.configurator .wrapper').load('./changelog.html');

        if (callback) callback();
    });
};

TABS.debug.cleanup = function (callback) {
    if (callback) callback();
};