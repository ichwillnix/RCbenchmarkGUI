'use strict';

// Google Analytics
var googleAnalyticsService = analytics.getService('ice_cream_app');
var googleAnalytics = googleAnalyticsService.getTracker('UA-62215173-1');
var googleAnalyticsConfig = false;
googleAnalyticsService.getConfig().addCallback(function (config) {
    googleAnalyticsConfig = config;
});

// shows/hides html elements based on current board version
function showBoardSpecific(){
    if(CONFIG.boardVersion === "Series 1580"){
        $(".1580").show();
        $(".1520").hide();
    }else{
        $(".1580").hide();
        $(".1520").show();
    }
    updateLeftPanelView();
    logSample.refreshHeader();
    logSample.newLog();
}

$(document).ready(loadPersistentVariables); //$(document).ready()

function loadPersistentVariables () {
    localize();
    
    // Load from memory
    // See memorymanagement.js. Not necessary to add initial memory load anymore.
    
    for (var attrname in CONFIG) {
        if(typeof CONFIG[attrname] !== "undefined") {
            if (CONFIG[attrname].hasOwnProperty("isStoredVariable")) {
            initialMemoryLoad(function () {}, CONFIG[attrname]);
            }
        }                
    }
        
    
    showBoardSpecific();
    
    // alternative - window.navigator.appVersion.match(/Chrome\/([0-9.]*)/)[1];
    GUI.log('Running - OS: <strong>' + GUI.operating_system + '</strong>, ' +
        'Chrome: <strong>' + window.navigator.appVersion.replace(/.*Chrome\/([0-9.]*).*/, "$1") + '</strong>, ' +
        'Configurator: <strong>' + chrome.runtime.getManifest().version + '</strong>');

    $('#status-bar .version').text(chrome.runtime.getManifest().version);

    display_text();

    // log library versions in console to make version tracking easier
    console.log('Libraries: jQuery - ' + $.fn.jquery + ', d3 - ' + d3.version);
	
	//get user limits
	chrome.storage.local.get('USER_LIMITS', function (result) {
		if (result.USER_LIMITS != undefined){
            //if stored object doesn't have some values (eg. new sensors added in update), copy their default limit
            for (var attrname in USER_LIMITS) { 
              if(result.USER_LIMITS[attrname]===undefined){
                result.USER_LIMITS[attrname] = USER_LIMITS[attrname]; 
              }
            }
			USER_LIMITS = result.USER_LIMITS;
		}
        
        //get system limits (in case of hack code was used)
        chrome.storage.local.get('SYSTEM_LIMITS', function (result) {
            if (result.SYSTEM_LIMITS != undefined){
                //if stored object doesn't have some values (eg. new sensors added in update), copy their default limit
                for (var attrname in SYSTEM_LIMITS) { 
                  if(result.SYSTEM_LIMITS[attrname]===undefined){
                    result.SYSTEM_LIMITS[attrname] = SYSTEM_LIMITS[attrname]; 
                  }
                }
                SYSTEM_LIMITS = result.SYSTEM_LIMITS;
            }
            refreshCutoffs();
        });
	});

	//get tare values
	chrome.storage.local.get('TARE_OFFSET', function (result) {
		if (result.TARE_OFFSET != undefined){
			TARE_OFFSET = result.TARE_OFFSET;
			LPFLeft.update(-TARE_OFFSET.loadCellLeftResetValue/1000); //zero using tare value
			LPFRight.update(-TARE_OFFSET.loadCellRightResetValue/1000);
			LPFThrust.update(-TARE_OFFSET.loadCellThrustResetValue/1000);
		}	
	});

	//pole number settings
    // getPoles();

	//load scripting setting
	chrome.storage.local.get('SCRIPTING_MODE', function (result) {
		if (result.SCRIPTING_MODE != undefined){
			CONFIG.scripting_mode = result.SCRIPTING_MODE;
		}
		loadScriptingTab();	
	});
	
	//get user display units
	chrome.storage.local.get('UNITS_DISPLAY', function (result) {
		for (var attrname in result.UNITS_DISPLAY) { 
            // Overwrite the property if it existed in memory.
            UNITS.display[attrname] = result.UNITS_DISPLAY[attrname]; 
        }
	});

	refreshCutoffs();
	
	//resize
	function resizeBarCalc(){
		$('#content').height(event.clientY-22);
		$('#plot').height($(window).height() - $('#content').height() - 43);
	}	
	var resizing = false;
	resizeBarCalc();
	$("#resize-bar").mousedown(recalculateOnResize);
    $( window ).resize(recalculateOnResize);
    
    function recalculateOnResize() {
		resizing = true;
		$("#main-wrapper").mousemove(function () {
			if(resizing){
				$("#content, #left-pane").css('pointer-events', 'none');
				$("#main-wrapper").css('cursor', 'row-resize');
				resizeBarCalc();
			}
		});	
		$(window).mouseup(function () {
			$("#content, #left-pane").css('pointer-events', 'auto');
			$("#main-wrapper").css('cursor', 'default');
			resizing = false;
			try{
				//Try to refresh the code editor size if any
				ace.edit("editor").resize(true);
			}catch (e){

			}
		});
	}
        

	//Called when outside of GUI is resized
	$(window).resize(resizeBarCalc);
	
    // Tabs
    var ui_tabs = $('#tabs > ul');
    $('a', ui_tabs).click(function () {
        if ($(this).parent().hasClass('active') == false && !GUI.tab_switch_in_progress) { // only initialize when the tab isn't already active
            var self = this,
                tabClass = $(self).parent().prop('class');

            var tabRequiresConnection = $(self).parent().hasClass('mode-connected');
            
            var tab = tabClass.substring(4);
            var tabName = $(self).text();
            
            if (tabRequiresConnection && !CONFIGURATOR.connectionValid) {
                GUI.log(chrome.i18n.getMessage('tabSwitchConnectionRequired'));
                return;
            }
            
            if (GUI.connect_lock) { // tab switching disabled while operation is in progress
                GUI.log(chrome.i18n.getMessage('tabSwitchWaitForOperation'));
                return;
            }
            
            if (GUI.allowedTabs.indexOf(tab) < 0) {
                GUI.log(chrome.i18n.getMessage('tabSwitchUpgradeRequired', [tabName]));
                return;
            }

            GUI.tab_switch_in_progress = true;

            GUI.tab_switch_cleanup(function () {
                // disable previously active tab highlight
                $('li', ui_tabs).removeClass('active');
                if(tab == 'sensors') {
					$('li.tab_motors').addClass('active');
				}

                // Highlight selected tab
                $(self).parent().addClass('active');

                function content_ready() {
                    GUI.tab_switch_in_progress = false;
                    $("div#tabAutomatic").hide();
					$("div#tabContent").show();
                }
                //loads tab content
                if(tab === 'autocontrol'){
					$("div#tabAutomatic").show();
					$("div#tabContent").hide();

					//Reset outputs
					OUTPUT_DATA = {
						ESC_PWM:        1000,
						Servo_PWM:      [1500,1500,1500],
						active:         [0,0,0,0]
					};

					TABS[tab].initialize(function(){GUI.tab_switch_in_progress = false;});
                }else{
					if(tab != 'undefined'){
						if (tab == 'landing'){
						   TABS.landing.initialize(content_ready);
							 if($.trim($('#plot').html())==''){
								 TABS.sensors.initialize(content_ready);
							 }
						}else
							TABS[tab].initialize(content_ready);
					} else console.log('Tab not found:' + tab);
                }
            });
        }
    });

    $('#tabs ul.mode-disconnected li a:first').click();

    // listen to all input change events and adjust the value within limits if necessary
    $("#content").on('focus', 'input[type="number"]', function () {
        var element = $(this),
            val = element.val();

        if (!isNaN(val)) {
            element.data('previousValue', parseFloat(val));
        }
    });

    $("#content").on('keydown', 'input[type="number"]', function (e) {
        // whitelist all that we need for numeric control
        var whitelist = [
            96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, // numpad and standard number keypad
            109, 189, // minus on numpad and in standard keyboard
            8, 46, 9, // backspace, delete, tab
            190, 110, // decimal point
            37, 38, 39, 40, 13 // arrows and enter
        ];

        if (whitelist.indexOf(e.keyCode) == -1) {
            e.preventDefault();
        }
    });

    $("#content").on('change', 'input[type="number"]', function () {
        var element = $(this),
            min = parseFloat(element.prop('min')),
            max = parseFloat(element.prop('max')),
            step = parseFloat(element.prop('step')),
            val = parseFloat(element.val()),
            decimal_places;

        // only adjust minimal end if bound is set
        if (element.prop('min')) {
            if (val < min) {
                element.val(min);
                val = min;
            }
        }

        // only adjust maximal end if bound is set
        if (element.prop('max')) {
            if (val > max) {
                element.val(max);
                val = max;
            }
        }

        // if entered value is illegal use previous value instead
        if (isNaN(val)) {
            element.val(element.data('previousValue'));
            val = element.data('previousValue');
        }

        // if step is not set or step is int and value is float use previous value instead
        if (isNaN(step) || step % 1 === 0) {
            if (val % 1 !== 0) {
                element.val(element.data('previousValue'));
                val = element.data('previousValue');
            }
        }

        // if step is set and is float and value is int, convert to float, keep decimal places in float according to step *experimental*
        if (!isNaN(step) && step % 1 !== 0) {
            decimal_places = String(step).split('.')[1].length;

            if (val % 1 === 0) {
                element.val(val.toFixed(decimal_places));
            } else if (String(val).split('.')[1].length != decimal_places) {
                element.val(val.toFixed(decimal_places));
            }
        }
    });

	//Keyboard shortcuts
    $('html').on('keydown' , function(event) {
		//Spacebar -> safety cutoff
		if(event.which == 32) {
			userCutoff = true;
			googleAnalytics.sendEvent('SpacebarCutoff', 'Click');
		}
        
        //Notify scripting engine
        if(event.which) {
            scriptSendMessage('keyboardPress', event.which);
        }
    });

    updateWorkingDirectory ();
    
}
function catch_startup_time(startTime) {
    var endTime = new Date().getTime(),
        timeSpent = endTime - startTime;

    googleAnalytics.sendTiming('Load Times', 'Application Startup', timeSpent);
}

function microtime() {
    var now = new Date().getTime() / 1000;

    return now;
}

function millitime() {
    var now = new Date().getTime();

    return now;
}

function bytesToSize(bytes) {
    if (bytes < 1024) {
        bytes = bytes + ' Bytes';
    } else if (bytes < 1048576) {
        bytes = (bytes / 1024).toFixed(3) + ' KB';
    } else if (bytes < 1073741824) {
        bytes = (bytes / 1048576).toFixed(3) + ' MB';
    } else {
        bytes = (bytes / 1073741824).toFixed(3) + ' GB';
    }

    return bytes;
}

Number.prototype.clamp = function(min, max) {
    return Math.min(Math.max(this, min), max);
};

/**
 * String formatting now supports currying (partial application).
 * For a format string with N replacement indices, you can call .format
 * with M <= N arguments. The result is going to be a format string
 * with N-M replacement indices, properly counting from 0 .. N-M.
 * The following Example should explain the usage of partial applied format:
 *  "{0}:{1}:{2}".format("a","b","c") === "{0}:{1}:{2}".format("a","b").format("c")
 *  "{0}:{1}:{2}".format("a").format("b").format("c") === "{0}:{1}:{2}".format("a").format("b", "c")
 **/
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (t, i) {
        return args[i] !== void 0 ? args[i] : "{"+(i-args.length)+"}";
    });
};

// ref: http://stackoverflow.com/a/1293163/2343
// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray( strData, strDelimiter ){
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );


    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;


    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec( strData )){

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push( [] );

        }

        var strMatchedValue;

        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[ 2 ]){

            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            // We found a non-quoted value.
            strMatchedValue = arrMatches[ 3 ];

        }


        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}