'use strict';

var gulp = require('gulp'),
    clean = require('gulp-clean'),
    cleanhtml = require('gulp-cleanhtml'),
    lintspaces = require('gulp-lintspaces'),
    sassRuby = require('gulp-ruby-sass'),
    concatCss = require('gulp-concat-css'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    eslint = require('gulp-eslint'),
    zip = require('gulp-zip');

// clean build directory
gulp.task('clean', function cleanTask() {
    return gulp.src('build/*', {
        read: false
    })
    .pipe(clean());
});

// copy all non js/html content to build directory
// except "scripts" which should not be minified
// except "libraries" which are already minified
gulp.task('copy', function copyTask() {
    gulp.src(['src/js/libraries/**/*.js'])
        .pipe(gulp.dest('build/js/libraries'));
    gulp.src(['src/scripts/*.js'])
        .pipe(gulp.dest('build/scripts'));
    return gulp.src(['src/**', '!src/**/*.js', '!src/**/*.html', '!src/tabs/autocontrol/rcbApi/out/**'])
        .pipe(gulp.dest('build'));
});

// copy and compress HTML files
gulp.task('html', function htmlTask() {
    return gulp.src(['src/**/*.html', '!src/tabs/autocontrol/rcbApi/out/**'])
        .pipe(cleanhtml())
        .pipe(gulp.dest('build/'));
});

// process javascript, except the "scripts" and "libraries"
gulp.task('js', function jsAllTask() {
    return gulp.src(['src/**/*.js', '!src/scripts/*.js','!src/tabs/autocontrol/rcbApi/out/**','!src/js/libraries/**/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest('./build/'));
});

gulp.task('build', ['html', 'js', 'copy'], function buildTask() {

});

// default task
gulp.task('default', ['clean'], function defaultTask() {
    gulp.start('build');
});
